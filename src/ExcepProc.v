module ExcepProc(
  input   [1:0]     prcn,
  input   [63:0]    op_A,
  input   [63:0]    op_B,
  input   [63:0]    op_C,
  output  [3:0]     nan_flag,
  output  [63:0]    nan_res,
  output  [3:0]     inf_flag,
  output  [63:0]    inf_res
);

  wire              nan_flag_d, inf_flag_d;
  wire    [1:0]     nan_flag_s, inf_flag_s;
  wire    [3:0]     nan_flag_h, inf_flag_h;
  wire    [63:0]    nan_res_d, nan_res_s, nan_res_h;
  wire    [63:0]    inf_res_d, inf_res_s, inf_res_h;

  ExcepUnit #(
    .DATA_WIDTH     (64),
    .EXP_WIDTH      (11),
    .MANT_WIDTH     (52)
  )
  u_ExcepUnit_d_0(
    .op_A           (op_A),
    .op_B           (op_B),
    .op_C           (op_C),
    .nan_flag       (nan_flag_d),
    .nan_res        (nan_res_d),
    .inf_flag       (inf_flag_d),
    .inf_res        (inf_res_d)
  );

  ExcepUnit #(
    .DATA_WIDTH     (32),
    .EXP_WIDTH      (8),
    .MANT_WIDTH     (23)
  )
  u_ExcepUnit_s_1(
    .op_A           (op_A[63:32]),
    .op_B           (op_B[63:32]),
    .op_C           (op_C[63:32]),
    .nan_flag       (nan_flag_s[1]),
    .nan_res        (nan_res_s[63:32]),
    .inf_flag       (inf_flag_s[1]),
    .inf_res        (inf_res_s[63:32])
  );

  ExcepUnit #(
    .DATA_WIDTH     (32),
    .EXP_WIDTH      (8),
    .MANT_WIDTH     (23)
  )
  u_ExcepUnit_s_0(
    .op_A           (op_A[31:0]),
    .op_B           (op_B[31:0]),
    .op_C           (op_C[31:0]),
    .nan_flag       (nan_flag_s[0]),
    .nan_res        (nan_res_s[31:0]),
    .inf_flag       (inf_flag_s[0]),
    .inf_res        (inf_res_s[31:0])
  );

  ExcepUnit #(
    .DATA_WIDTH     (16),
    .EXP_WIDTH      (5),
    .MANT_WIDTH     (10)
  )
  u_ExcepUnit_h_3(
    .op_A           (op_A[63:48]),
    .op_B           (op_B[63:48]),
    .op_C           (op_C[63:48]),
    .nan_flag       (nan_flag_h[3]),
    .nan_res        (nan_res_h[63:48]),
    .inf_flag       (inf_flag_h[3]),
    .inf_res        (inf_res_h[63:48])
  );

  ExcepUnit #(
    .DATA_WIDTH     (16),
    .EXP_WIDTH      (5),
    .MANT_WIDTH     (10)
  )
  u_ExcepUnit_h_2(
    .op_A           (op_A[47:32]),
    .op_B           (op_B[47:32]),
    .op_C           (op_C[47:32]),
    .nan_flag       (nan_flag_h[2]),
    .nan_res        (nan_res_h[47:32]),
    .inf_flag       (inf_flag_h[2]),
    .inf_res        (inf_res_h[47:32])
  );

  ExcepUnit #(
    .DATA_WIDTH     (16),
    .EXP_WIDTH      (5),
    .MANT_WIDTH     (10)
  )
  u_ExcepUnit_h_1(
    .op_A           (op_A[31:16]),
    .op_B           (op_B[31:16]),
    .op_C           (op_C[31:16]),
    .nan_flag       (nan_flag_h[1]),
    .nan_res        (nan_res_h[31:16]),
    .inf_flag       (inf_flag_h[1]),
    .inf_res        (inf_res_h[31:16])
  );

  ExcepUnit #(
    .DATA_WIDTH     (16),
    .EXP_WIDTH      (5),
    .MANT_WIDTH     (10)
  )
  u_ExcepUnit_h_0(
    .op_A           (op_A[15:0]),
    .op_B           (op_B[15:0]),
    .op_C           (op_C[15:0]),
    .nan_flag       (nan_flag_h[0]),
    .nan_res        (nan_res_h[15:0]),
    .inf_flag       (inf_flag_h[0]),
    .inf_res        (inf_res_h[15:0])
  );

  assign nan_flag = prcn[1] ? {4{nan_flag_d}} : (prcn[0] ? {{2{nan_flag_s[1]}}, {2{nan_flag_s[0]}}} : nan_flag_h);
  assign nan_res  = prcn[1] ? nan_res_d : (prcn[0] ? nan_res_s : nan_res_h);
  assign inf_flag = prcn[1] ? {4{inf_flag_d}} : (prcn[0] ? {{2{inf_flag_s[1]}}, {2{inf_flag_s[0]}}} : inf_flag_h);
  assign inf_res  = prcn[1] ? inf_res_d : (prcn[0] ? inf_res_s : inf_res_h);

endmodule