module ConstShifter(
  input   [1:0]       prcn,
  input   [3:0]       sh_en,
  input   [162:0]     data_in,
  input   [3:0]       sh_less,     // shift 1 bit less
  output  [162:0]     data_out,
  output  [3:0]       exp_corr
);

  wire  [162:0]     data_out_d, data_out_s, data_out_h;
  wire  [3:0]       exp_corr_d, exp_corr_s, exp_corr_h;

  assign data_out_d           = sh_en[3] ? (exp_corr[3] ? (data_in<<54)          : (data_in<<55))          : data_in;
  assign data_out_s[162:81]   = sh_en[3] ? (exp_corr[3] ? (data_in[162:81]<<25)  : (data_in[162:81]<<26))  : data_in[162:81];
  assign data_out_s[80:0]     = sh_en[1] ? (exp_corr[1] ? (data_in[80:0]<<25)    : (data_in[80:0]<<26))    : data_in[80:0];
  assign data_out_h[162:121]  = sh_en[3] ? (exp_corr[3] ? (data_in[162:121]<<12) : (data_in[162:121]<<13)) : data_in[162:121];
  assign data_out_h[120:81]   = sh_en[2] ? (exp_corr[2] ? (data_in[120:81]<<12)  : (data_in[120:81]<<13))  : data_in[120:81];
  assign data_out_h[80:40]    = sh_en[1] ? (exp_corr[1] ? (data_in[80:40]<<12)   : (data_in[80:40]<<13))   : data_in[80:40];
  assign data_out_h[39:0]     = sh_en[0] ? (exp_corr[0] ? (data_in[39:0]<<12)    : (data_in[39:0]<<13))    : data_in[39:0];
  assign data_out             = prcn[1] ? data_out_d : (prcn[0] ? data_out_s : data_out_h);

  assign exp_corr_d = {4{data_in[108]}};
  assign exp_corr_s = {{2{data_in[137]}}, {2{data_in[55]}}};
  assign exp_corr_h = {data_in[150], data_in[108], data_in[68], data_in[27]};
  assign exp_corr   = (prcn[1] ? exp_corr_d : (prcn[0] ? exp_corr_s : exp_corr_h)) | sh_less;

endmodule