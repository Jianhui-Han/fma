module FMAS3(
  input                 clk,
  input                 rst_n,
  input       [1:0]     prcn,
  input       [2:0]     rnd_mode,
  input                 vld_in,
  input       [162:0]   sum_comp,
  input       [3:0]     sig_fma,
  input       [22:0]    lzac_cnt,
  input       [3:0]     lzac_vld,
  input       [36:0]    exp_tmp,
  input       [3:0]     sticky,
  input       [26:0]    sh_amt,
  output reg            vld_out,
  output reg  [63:0]    result,
  input       [3:0]     nan_flag,
  input       [63:0]    nan_res,
  input       [3:0]     inf_flag,
  input       [63:0]    inf_res,
  input       [3:0]     subtract
);

  wire  [55:0]      sum_norm;
  wire  [28:0]      exp_norm;
  wire  [51:0]      mant_fma;
  wire  [28:0]      exp_fma;
  wire  [63:0]      res_fma;
  wire  [3:0]       of_flag;

  NormShifter u_NormShifter(
    .prcn           (prcn),
    .sum_comp       (sum_comp),
    .lzac_cnt       (lzac_cnt),
    .lzac_vld       (lzac_vld),
    .exp_tmp        (exp_tmp),
    .sh_amt         (sh_amt),
    .sum_norm       (sum_norm),
    .exp_norm       (exp_norm),
    .of_flag        (of_flag),
    .subtract       (subtract)
  );

  Rounder u_Rounder(
    .prcn           (prcn),
    .rnd_mode       (rnd_mode),
    .sum_norm       (sum_norm),
    .exp_norm       (exp_norm),
    .sig_fma        (sig_fma),
    .mant_fma       (mant_fma),
    .exp_fma        (exp_fma),
    .sticky         (sticky),
    .subtract       (subtract)
  );

  OutputProc u_OutputProc(
    .prcn           (prcn),
    .mant_fma       (mant_fma),
    .exp_fma        (exp_fma),
    .sig_fma        (sig_fma),
    .res_fma        (res_fma),
    .of_flag        (of_flag),
    .nan_flag       (nan_flag),
    .nan_res        (nan_res),
    .inf_flag       (inf_flag),
    .inf_res        (inf_res)
  );

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      vld_out <= 1'd0;
      result  <= 64'd0;
    end
    else begin
      vld_out <= vld_in;
      result  <= res_fma;
    end
  end

endmodule