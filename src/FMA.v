module FMA(
  input             clk,
  input             rst_n,
  input   [1:0]     prcn,
  input   [2:0]     rnd_mode,
  input             vld_in,
  input   [63:0]    op_A,
  input   [63:0]    op_B,
  input   [63:0]    op_C,
  output            vld_out,
  output  [63:0]    result,
  output            busy
);

  wire              s1_s2_vld;
  wire  [26:0]      s1_s3_sh_amt;
  wire  [3:0]       s1_s2_sig_tmp;
  wire  [36:0]      s1_s3_exp_tmp;
  wire  [162:0]     s1_s2_addend;
  wire  [3:0]       s1_s3_sticky;
  wire  [107:0]     s1_s2_prod_sum;
  wire  [107:0]     s1_s2_prod_carry;
  wire  [3:0]       s1_s2_subtract;
  wire  [3:0]       s1_s3_nan_flag;
  wire  [63:0]      s1_s3_nan_res;
  wire  [3:0]       s1_s3_inf_flag;
  wire  [63:0]      s1_s3_inf_res;

  wire              s2_s3_vld;
  wire  [162:0]     s2_s3_sum_comp;
  wire  [3:0]       s2_s3_sig_fma;
  wire  [22:0]      s2_s3_lzac_cnt;
  wire  [3:0]       s2_s3_lzac_vld;
  wire  [3:0]       s2_s3_sh_adj_s2;
  wire  [3:0]       s2_s3_subtract;

  reg   [1:0]       prcn_s2, prcn_s3;
  reg   [2:0]       rnd_mode_s2, rnd_mode_s3;

  FMAS1 u_FMAS1(
    .clk            (clk),
    .rst_n          (rst_n),
    .prcn           (prcn),
    .vld_in         (vld_in),
    .op_A           (op_A),
    .op_B           (op_B),
    .op_C           (op_C),
    .vld_out        (s1_s2_vld),
    .sh_amt         (s1_s3_sh_amt),
    .sig_tmp        (s1_s2_sig_tmp),
    .exp_tmp        (s1_s3_exp_tmp),
    .addend         (s1_s2_addend),
    .subtract       (s1_s2_subtract),
    .sticky         (s1_s3_sticky),
    .prod_sum       (s1_s2_prod_sum),
    .prod_carry     (s1_s2_prod_carry),
    .nan_flag       (s1_s3_nan_flag),
    .nan_res        (s1_s3_nan_res),
    .inf_flag       (s1_s3_inf_flag),
    .inf_res        (s1_s3_inf_res)
  );

  FMAS2 u_FMAS2(
    .clk            (clk),
    .rst_n          (rst_n),
    .prcn           (prcn_s2),
    .rnd_mode       (rnd_mode_s2),
    .vld_in         (s1_s2_vld),
    .prod_sum       (s1_s2_prod_sum),
    .prod_carry     (s1_s2_prod_carry),
    .addend         (s1_s2_addend),
    .subtract       (s1_s2_subtract),
    .sig_tmp        (s1_s2_sig_tmp),
    .vld_out        (s2_s3_vld),
    .sum_comp       (s2_s3_sum_comp),
    .sig_fma        (s2_s3_sig_fma),
    .lzac_cnt       (s2_s3_lzac_cnt),
    .lzac_vld       (s2_s3_lzac_vld),
    .subt_out       (s2_s3_subtract)
  );

  FMAS3 u_FMAS3(
    .clk            (clk),
    .rst_n          (rst_n),
    .prcn           (prcn_s3),
    .rnd_mode       (rnd_mode_s3),
    .vld_in         (s2_s3_vld),
    .sum_comp       (s2_s3_sum_comp),
    .sig_fma        (s2_s3_sig_fma),
    .lzac_cnt       (s2_s3_lzac_cnt),
    .lzac_vld       (s2_s3_lzac_vld),
    .exp_tmp        (s1_s3_exp_tmp),
    .sticky         (s1_s3_sticky),
    .sh_amt         (s1_s3_sh_amt),
    .vld_out        (vld_out),
    .result         (result),
    .nan_flag       (s1_s3_nan_flag),
    .nan_res        (s1_s3_nan_res),
    .inf_flag       (s1_s3_inf_flag),
    .inf_res        (s1_s3_inf_res),
    .subtract       (s2_s3_subtract)
  );

  assign busy = vld_in | s1_s2_vld | s2_s3_vld;

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      prcn_s2     <= 2'b11;
      prcn_s3     <= 2'b11;
      rnd_mode_s2 <= 3'b000;
      rnd_mode_s3 <= 3'b000;
    end
    else begin
      prcn_s2     <= prcn;
      prcn_s3     <= prcn_s2;
      rnd_mode_s2 <= rnd_mode;
      rnd_mode_s3 <= rnd_mode_s2;
    end
  end

endmodule