module LeftShifter(
  input   [1:0]         prcn,
  input   [162:0]       data_in,
  input   [22:0]        sh_amt,
  output  [162:0]       data_out,
  output  [3:0]         data_left
);

  wire  [39:0]    data_in_0_0, data_out_0_0, data_sh_0_0;
  wire  [40:0]    data_in_0_1, data_out_0_1, data_sh_0_1;
  wire  [39:0]    data_in_0_2, data_out_0_2, data_sh_0_2;
  wire  [41:0]    data_in_0_3, data_out_0_3, data_sh_0_3;
  wire            shift_in_0_1, shift_in_0_2, shift_in_0_3;
  wire  [39:0]    data_out_1_0, data_sh_1_0;
  wire  [40:0]    data_out_1_1, data_sh_1_1;
  wire  [39:0]    data_out_1_2, data_sh_1_2;
  wire  [41:0]    data_out_1_3, data_sh_1_3;
  wire  [1:0]     shift_in_1_1, shift_in_1_2, shift_in_1_3;
  wire  [39:0]    data_out_2_0, data_sh_2_0;
  wire  [40:0]    data_out_2_1, data_sh_2_1;
  wire  [39:0]    data_out_2_2, data_sh_2_2;
  wire  [41:0]    data_out_2_3, data_sh_2_3;
  wire  [3:0]     shift_in_2_1, shift_in_2_2, shift_in_2_3;
  wire  [39:0]    data_out_3_0, data_sh_3_0;
  wire  [40:0]    data_out_3_1, data_sh_3_1;
  wire  [39:0]    data_out_3_2, data_sh_3_2;
  wire  [41:0]    data_out_3_3, data_sh_3_3;
  wire  [7:0]     shift_in_3_1, shift_in_3_2, shift_in_3_3;
  wire  [39:0]    data_out_4_0, data_sh_4_0;
  wire  [40:0]    data_out_4_1, data_sh_4_1;
  wire  [39:0]    data_out_4_2, data_sh_4_2;
  wire  [41:0]    data_out_4_3, data_sh_4_3;
  wire  [15:0]    shift_in_4_1, shift_in_4_2, shift_in_4_3;
  wire  [80:0]    data_in_5_0, data_out_5_0, data_sh_5_0;
  wire  [81:0]    data_in_5_1, data_out_5_1, data_sh_5_1;
  wire  [31:0]    shift_in_5_1;
  wire  [162:0]   data_in_6_0, data_out_6_0, data_sh_6_0;

  wire  [3:0]     left_0;
  wire  [3:0]     left_1;
  wire  [3:0]     left_2;
  wire  [3:0]     left_3;
  wire  [3:0]     left_4;
  wire  [1:0]     left_5;
  wire            left_6;

  // level 0
  assign data_in_0_0  = data_in[39:0];
  assign data_sh_0_0  = {data_in_0_0[38:0], 1'd0};
  assign data_out_0_0 = sh_amt[0] ? data_sh_0_0 : data_in_0_0;
  assign data_in_0_1  = data_in[80:40];
  assign shift_in_0_1 = (prcn == 2'b00) ? 1'd0 : data_in_0_0[39];
  assign data_sh_0_1  = {data_in_0_1[39:0], shift_in_0_1};
  assign data_out_0_1 = sh_amt[5] ? data_sh_0_1 : data_in_0_1;
  assign data_in_0_2  = data_in[120:81];
  assign shift_in_0_2 = prcn[1] ? data_in_0_1[40] : 1'd0;
  assign data_sh_0_2  = {data_in_0_2[38:0], shift_in_0_2};
  assign data_out_0_2 = sh_amt[11] ? data_sh_0_2 : data_in_0_2;
  assign data_in_0_3  = data_in[162:121];
  assign shift_in_0_3 = (prcn == 2'b00) ? 1'd0 : data_in_0_2[39];
  assign data_sh_0_3  = {data_in_0_3[40:0], shift_in_0_3};
  assign data_out_0_3 = sh_amt[16] ? data_sh_0_3 : data_in_0_3;

  assign left_0[0]    = sh_amt[0]  ? data_in_0_0[39] : 1'd0;
  assign left_0[1]    = sh_amt[5]  ? data_in_0_1[40] : 1'd0;
  assign left_0[2]    = sh_amt[11] ? data_in_0_2[39] : 1'd0;
  assign left_0[3]    = sh_amt[16] ? data_in_0_3[41] : 1'd0;

  // level 1
  assign data_sh_1_0  = {data_out_0_0[37:0], 2'd0};
  assign data_out_1_0 = sh_amt[1] ? data_sh_1_0 : data_out_0_0;
  assign shift_in_1_1 = (prcn == 2'b00) ? 2'd0 : data_out_0_0[39:38];
  assign data_sh_1_1  = {data_out_0_1[38:0], shift_in_1_1};
  assign data_out_1_1 = sh_amt[6] ? data_sh_1_1 : data_out_0_1;
  assign shift_in_1_2 = prcn[1] ? data_out_0_1[40:39] : 2'd0;
  assign data_sh_1_2  = {data_out_0_2[37:0], shift_in_1_2};
  assign data_out_1_2 = sh_amt[12] ? data_sh_1_2 : data_out_0_2;
  assign shift_in_1_3 = (prcn == 2'b00) ? 2'd0 : data_out_0_2[39:38];
  assign data_sh_1_3  = {data_out_0_3[39:0], shift_in_1_3};
  assign data_out_1_3 = sh_amt[17] ? data_sh_1_3 : data_out_0_3;

  assign left_1[0]    = sh_amt[1]  ? data_out_0_0[38] : left_0[0];
  assign left_1[1]    = sh_amt[6]  ? data_out_0_1[39] : left_0[1];
  assign left_1[2]    = sh_amt[12] ? data_out_0_2[38] : left_0[2];
  assign left_1[3]    = sh_amt[17] ? data_out_0_3[40] : left_0[3];

  // level 2
  assign data_sh_2_0  = {data_out_1_0[35:0], 4'd0};
  assign data_out_2_0 = sh_amt[2] ? data_sh_2_0 : data_out_1_0;
  assign shift_in_2_1 = (prcn == 2'b00) ? 4'd0 : data_out_1_0[39:36];
  assign data_sh_2_1  = {data_out_1_1[36:0], shift_in_2_1};
  assign data_out_2_1 = sh_amt[7] ? data_sh_2_1 : data_out_1_1;
  assign shift_in_2_2 = prcn[1] ? data_out_1_1[40:37] : 4'd0;
  assign data_sh_2_2  = {data_out_1_2[35:0], shift_in_2_2};
  assign data_out_2_2 = sh_amt[13] ? data_sh_2_2 : data_out_1_2;
  assign shift_in_2_3 = (prcn == 2'b00) ? 4'd0 : data_out_1_2[39:36];
  assign data_sh_2_3  = {data_out_1_3[37:0], shift_in_2_3};
  assign data_out_2_3 = sh_amt[18] ? data_sh_2_3 : data_out_1_3;

  assign left_2[0]    = sh_amt[2]  ? data_out_1_0[36] : left_1[0];
  assign left_2[1]    = sh_amt[7]  ? data_out_1_1[37] : left_1[1];
  assign left_2[2]    = sh_amt[13] ? data_out_1_2[36] : left_1[2];
  assign left_2[3]    = sh_amt[18] ? data_out_1_3[38] : left_1[3];

  // level 3
  assign data_sh_3_0  = {data_out_2_0[31:0], 8'd0};
  assign data_out_3_0 = sh_amt[3] ? data_sh_3_0 : data_out_2_0;
  assign shift_in_3_1 = (prcn == 2'b00) ? 8'd0 : data_out_2_0[39:32];
  assign data_sh_3_1  = {data_out_2_1[32:0], shift_in_3_1};
  assign data_out_3_1 = sh_amt[8] ? data_sh_3_1 : data_out_2_1;
  assign shift_in_3_2 = prcn[1] ? data_out_2_1[40:33] : 8'd0;
  assign data_sh_3_2  = {data_out_2_2[31:0], shift_in_3_2};
  assign data_out_3_2 = sh_amt[14] ? data_sh_3_2 : data_out_2_2;
  assign shift_in_3_3 = (prcn == 2'b00) ? 8'd0 : data_out_2_2[39:32];
  assign data_sh_3_3  = {data_out_2_3[33:0], shift_in_3_3};
  assign data_out_3_3 = sh_amt[19] ? data_sh_3_3 : data_out_2_3;

  assign left_3[0]    = sh_amt[3]  ? data_out_2_0[32] : left_2[0];
  assign left_3[1]    = sh_amt[8]  ? data_out_2_1[33] : left_2[1];
  assign left_3[2]    = sh_amt[14] ? data_out_2_2[32] : left_2[2];
  assign left_3[3]    = sh_amt[19] ? data_out_2_3[34] : left_2[3];

  // level 4
  assign data_sh_4_0  = {data_out_3_0[23:0], 16'd0};
  assign data_out_4_0 = sh_amt[4] ? data_sh_4_0 : data_out_3_0;
  assign shift_in_4_1 = (prcn == 2'b00) ? 16'd0 : data_out_3_0[39:24];
  assign data_sh_4_1  = {data_out_3_1[24:0], shift_in_4_1};
  assign data_out_4_1 = sh_amt[9] ? data_sh_4_1 : data_out_3_1;
  assign shift_in_4_2 = prcn[1] ? data_out_3_1[40:25] : 16'd0;
  assign data_sh_4_2  = {data_out_3_2[23:0], shift_in_4_2};
  assign data_out_4_2 = sh_amt[15] ? data_sh_4_2 : data_out_3_2;
  assign shift_in_4_3 = (prcn == 2'b00) ? 16'd0 : data_out_3_2[39:24];
  assign data_sh_4_3  = {data_out_3_3[25:0], shift_in_4_3};
  assign data_out_4_3 = sh_amt[20] ? data_sh_4_3 : data_out_3_3;

  assign left_4[0]    = sh_amt[4]  ? data_out_3_0[24] : left_3[0];
  assign left_4[1]    = sh_amt[9]  ? data_out_3_1[25] : left_3[1];
  assign left_4[2]    = sh_amt[15] ? data_out_3_2[24] : left_3[2];
  assign left_4[3]    = sh_amt[20] ? data_out_3_3[26] : left_3[3];

  // level 5
  assign data_in_5_0  = {data_out_4_1, data_out_4_0};
  assign data_sh_5_0  = {data_in_5_0[48:0], 32'd0};
  assign data_out_5_0 = sh_amt[10] ? data_sh_5_0 : data_in_5_0;
  assign data_in_5_1  = {data_out_4_3, data_out_4_2};
  assign shift_in_5_1 = prcn[1] ? data_in_5_0[80:49] : 32'd0;
  assign data_sh_5_1  = {data_in_5_1[49:0], shift_in_5_1};
  assign data_out_5_1 = sh_amt[21] ? data_sh_5_1 : data_in_5_1;

  assign left_5[0]    = sh_amt[10] ? data_in_5_0[49] : left_4[1];
  assign left_5[1]    = sh_amt[21] ? data_in_5_1[50] : left_4[3];

  // level 6
  assign data_in_6_0  = {data_out_5_1, data_out_5_0};
  assign data_sh_6_0  = {data_in_6_0[99:0], 64'd0};
  assign data_out_6_0 = sh_amt[22] ? data_sh_6_0 : data_in_6_0;

  assign left_6       = sh_amt[22] ? data_in_6_0[100] : left_5[1];

  // output
  assign data_out   = data_out_6_0;
  assign data_left  = {left_6, left_4[2], left_5[0], left_4[0]};

endmodule