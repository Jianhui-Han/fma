module CSA42 #(
  parameter WIDTH = 13
)(
  input   [WIDTH-1:0]   op_A,
  input   [WIDTH-1:0]   op_B,
  input   [WIDTH-1:0]   op_C,
  input   [WIDTH-1:0]   op_D,
  output  [WIDTH-1:0]   sum,
  output  [WIDTH-1:0]   carry
);

  wire  [WIDTH-1:0]   csa_0_sum;
  wire  [WIDTH-1:0]   csa_0_carry;

  CSA32 #(
    .WIDTH    (WIDTH)
  )
  u_CSA32_0(
    .op_A     (op_A),
    .op_B     (op_B),
    .op_C     (op_C),
    .sum      (csa_0_sum),
    .carry    (csa_0_carry)
  );

  CSA32 #(
    .WIDTH    (WIDTH)
  )
  u_CSA32_1(
    .op_A     (op_D),
    .op_B     (csa_0_sum),
    .op_C     ({csa_0_carry[WIDTH-2:0], 1'd0}),
    .sum      (sum),
    .carry    (carry)
  );

endmodule