module ExpProc #(
  parameter EXP_WIDTH = 13,
  parameter AMT_WIDTH = 8
)(
  input   [EXP_WIDTH-1:0]   exp_A,
  input   [EXP_WIDTH-1:0]   exp_B,
  input   [EXP_WIDTH-1:0]   exp_C,
  input   [EXP_WIDTH-1:0]   sh_const,
  input   [AMT_WIDTH-1:0]   sh_max,
  output  [EXP_WIDTH-1:0]   exp_tmp,
  output  [AMT_WIDTH-1:0]   sh_amt,
  input                     zero_AB
);

  wire    [EXP_WIDTH-1:0]   csa_A, csa_B;
  wire    [EXP_WIDTH-1:0]   csa_sum, csa_carry;

  wire    [EXP_WIDTH-1:0]   exp_tmp_0, exp_tmp_1;
  wire    [EXP_WIDTH-1:0]   sh_amt_0, sh_amt_1, sh_amt_2;

  wire    [EXP_WIDTH-1:0]   exp_C_0;

  CSA32 #(
    .WIDTH      (EXP_WIDTH)
  )
  u_CSA32(
    .op_A       (csa_A),
    .op_B       (csa_B),
    .op_C       (sh_const),
    .sum        (csa_sum),
    .carry      (csa_carry)
  );

  assign csa_A      = (exp_A == 'd0) ? 'd1 : exp_A;
  assign csa_B      = (exp_B == 'd0) ? 'd1 : exp_B;
  assign exp_tmp_0  = {1'd0, csa_sum} + {csa_carry, 1'd0};
  assign exp_C_0    = (exp_C == 'd0) ? 'd1 : exp_C;
  assign exp_tmp_1  = sh_amt_0[EXP_WIDTH-1] ? exp_C_0 : exp_tmp_0;
  assign exp_tmp    = zero_AB ? exp_C_0 : exp_tmp_1;
  
  assign sh_amt_0   = exp_tmp_0 - exp_C_0;
  assign sh_amt_1   = (sh_amt_0 > sh_max) ? sh_max : sh_amt_0[AMT_WIDTH-1:0];
  assign sh_amt_2   = sh_amt_0[EXP_WIDTH-1] ? 'd0 : sh_amt_1;
  assign sh_amt     = zero_AB ? 'd0 : sh_amt_2;

endmodule