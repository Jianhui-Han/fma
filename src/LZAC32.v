module LZAC32 #(
  parameter WIDTH = 32
)(
  input   [WIDTH:0]         sum,
  input   [WIDTH:0]         carry,
  output  [4:0]             count,
  output                    valid
);

  wire  [WIDTH:0]         T, K, G;
  wire  [WIDTH:0]         ind, ind_0, ind_1;

  LDCT32 #(
    .WIDTH        (WIDTH)
  )
  u_LDCT32(
    .ind          (ind[WIDTH:1]),
    .count        (count),
    .valid        (valid)
  );

  genvar i;
  generate
    for (i=1; i<WIDTH; i=i+1) begin : ind_gen
      assign ind_0[i] = T[i+1] & ((~K[i-1] & G[i]) | (~G[i-1] & K[i]));
      assign ind_1[i] = ~T[i+1] & ((~K[i-1] & K[i]) | (~G[i-1] & G[i]));
      assign ind[i]   = ind_0[i] | ind_1[i];
    end
  endgenerate

  assign ind[0]       = (T[1] & K[0]) | (~T[1] & G[0]);
  assign ind[WIDTH]   = ~T[WIDTH] & T[WIDTH-1];

  assign T      = sum ^ carry;
  assign G      = sum & carry;
  assign K      = (~sum) & (~carry);
  
endmodule