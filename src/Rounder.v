module Rounder(
  input   [1:0]     prcn,
  input   [2:0]     rnd_mode,
  input   [55:0]    sum_norm,
  input   [28:0]    exp_norm,
  input   [3:0]     sig_fma,
  output  [51:0]    mant_fma,
  output  [28:0]    exp_fma,
  input   [3:0]     sticky,
  input   [3:0]     subtract
);

  wire  [51:0]      mant_rnd_d_0;
  wire  [22:0]      mant_rnd_s_1, mant_rnd_s_0;
  wire  [9:0]       mant_rnd_h_3, mant_rnd_h_2, mant_rnd_h_1, mant_rnd_h_0;

  wire  [10:0]      exp_fma_d_0;
  wire  [7:0]       exp_fma_s_1, exp_fma_s_0;
  wire  [4:0]       exp_fma_h_3, exp_fma_h_2, exp_fma_h_1, exp_fma_h_0;

  wire  [51:0]      mant_fma_d, mant_fma_s, mant_fma_h;

  wire  [28:0]      exp_fma_d, exp_fma_s, exp_fma_h;

  RndUnit #(
    .MANT_WIDTH   (52),
    .EXP_WIDTH    (11)
  )
  u_RndUnit_d_0(
    .rnd_mode     (rnd_mode),
    .sig_fma      (sig_fma[3]),
    .exp_norm     (exp_norm[28:18]),
    .sum_norm     (sum_norm),
    .mant_rnd     (mant_rnd_d_0),
    .exp_fma      (exp_fma_d_0),
    .sticky       (sticky[0]),
    .subtract     (subtract[3])
  );

  RndUnit #(
    .MANT_WIDTH   (23),
    .EXP_WIDTH    (8)
  )
  u_RndUnit_s_1(
    .rnd_mode     (rnd_mode),
    .sig_fma      (sig_fma[3]),
    .exp_norm     (exp_norm[25:18]),
    .sum_norm     (sum_norm[54:28]),
    .mant_rnd     (mant_rnd_s_1),
    .exp_fma      (exp_fma_s_1),
    .sticky       (sticky[2]),
    .subtract     (subtract[3])
  );

  RndUnit #(
    .MANT_WIDTH   (23),
    .EXP_WIDTH    (8)
  )
  u_RndUnit_s_0(
    .rnd_mode     (rnd_mode),
    .sig_fma      (sig_fma[1]),
    .exp_norm     (exp_norm[12:5]),
    .sum_norm     (sum_norm[26:0]),
    .mant_rnd     (mant_rnd_s_0),
    .exp_fma      (exp_fma_s_0),
    .sticky       (sticky[0]),
    .subtract     (subtract[1])
  );

  RndUnit #(
    .MANT_WIDTH   (10),
    .EXP_WIDTH    (5)
  )
  u_RndUnit_h_3(
    .rnd_mode     (rnd_mode),
    .sig_fma      (sig_fma[3]),
    .exp_norm     (exp_norm[22:18]),
    .sum_norm     (sum_norm[55:42]),
    .mant_rnd     (mant_rnd_h_3),
    .exp_fma      (exp_fma_h_3),
    .sticky       (sticky[3]),
    .subtract     (subtract[3])
  );

  RndUnit #(
    .MANT_WIDTH   (10),
    .EXP_WIDTH    (5)
  )
  u_RndUnit_h_2(
    .rnd_mode     (rnd_mode),
    .sig_fma      (sig_fma[2]),
    .exp_norm     (exp_norm[17:13]),
    .sum_norm     (sum_norm[41:28]),
    .mant_rnd     (mant_rnd_h_2),
    .exp_fma      (exp_fma_h_2),
    .sticky       (sticky[2]),
    .subtract     (subtract[2])
  );

  RndUnit #(
    .MANT_WIDTH   (10),
    .EXP_WIDTH    (5)
  )
  u_RndUnit_h_1(
    .rnd_mode     (rnd_mode),
    .sig_fma      (sig_fma[1]),
    .exp_norm     (exp_norm[9:5]),
    .sum_norm     (sum_norm[27:14]),
    .mant_rnd     (mant_rnd_h_1),
    .exp_fma      (exp_fma_h_1),
    .sticky       (sticky[1]),
    .subtract     (subtract[1])
  );

  RndUnit #(
    .MANT_WIDTH   (10),
    .EXP_WIDTH    (5)
  )
  u_RndUnit_h_0(
    .rnd_mode     (rnd_mode),
    .sig_fma      (sig_fma[0]),
    .exp_norm     (exp_norm[4:0]),
    .sum_norm     (sum_norm[13:0]),
    .mant_rnd     (mant_rnd_h_0),
    .exp_fma      (exp_fma_h_0),
    .sticky       (sticky[0]),
    .subtract     (subtract[0])
  );

  assign mant_fma_d = mant_rnd_d_0;
  assign mant_fma_s = {3'd0, mant_rnd_s_1, 3'd0, mant_rnd_s_0};
  assign mant_fma_h = {3'd0, mant_rnd_h_3, 3'd0, mant_rnd_h_2, 3'd0, mant_rnd_h_1, 3'd0, mant_rnd_h_0};
  assign mant_fma   = prcn[1] ? mant_fma_d : (prcn[0] ? mant_fma_s : mant_fma_h);

  assign exp_fma_d  = {exp_fma_d_0, 18'd0}; 
  assign exp_fma_s  = {3'd0, exp_fma_s_1, 5'd0, exp_fma_s_0, 5'd0};
  assign exp_fma_h  = {6'd0, exp_fma_h_3, exp_fma_h_2, 3'd0, exp_fma_h_1, exp_fma_h_0};
  assign exp_fma    = prcn[1] ? exp_fma_d : (prcn[0] ? exp_fma_s : exp_fma_h);

endmodule