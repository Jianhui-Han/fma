module BoothEnc(
  input   [14:0]        op_A,
  input   [2:0]         op_B,
  output  [15:0]        pp_body,
  output                pp_sign
);

  wire    [14:0]        pp_body_zero;
  wire    [14:0]        pp_body_neg;

  assign zero     = (op_B == 3'b111) | (op_B == 3'b000);
  assign double   = (op_B == 3'b011) | (op_B == 3'b100);
  assign pp_sign  = op_B[2];
  assign pp_body_zero = zero ? 15'd0 : op_A;
  assign pp_body_neg  = pp_sign ? ~pp_body_zero : pp_body_zero;
  assign pp_body      = double ? {pp_body_neg, pp_sign} : {pp_sign, pp_body_neg};

endmodule