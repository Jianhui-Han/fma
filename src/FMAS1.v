module FMAS1(
  input                 clk,
  input                 rst_n,
  input       [1:0]     prcn,
  input                 vld_in,
  input       [63:0]    op_A,
  input       [63:0]    op_B,
  input       [63:0]    op_C,
  output reg            vld_out,
  output reg  [26:0]    sh_amt,
  output reg  [3:0]     sig_tmp,
  output reg  [36:0]    exp_tmp,
  output reg  [162:0]   addend,
  output reg  [3:0]     subtract,
  output reg  [3:0]     sticky,
  output reg  [107:0]   prod_sum,
  output reg  [107:0]   prod_carry,
  output reg  [3:0]     nan_flag,
  output reg  [63:0]    nan_res,
  output reg  [3:0]     inf_flag,
  output reg  [63:0]    inf_res
);

  wire  [59:0]      mult_A, mult_B;
  wire  [162:0]     align_C;
  wire  [26:0]      sh_amt_int;
  wire  [3:0]       subtract_int;
  wire  [3:0]       sig_tmp_int;
  wire  [36:0]      exp_tmp_int;
  wire  [162:0]     addend_int;
  wire  [3:0]       sticky_int;
  wire  [107:0]     prod_sum_int, prod_carry_int;

  reg   [26:0]      sh_amt_d1;
  reg   [36:0]      exp_tmp_d1;
  reg   [3:0]       sticky_d1;

  wire  [3:0]       nan_flag_int;
  wire  [63:0]      nan_res_int;
  wire  [3:0]       inf_flag_int;
  wire  [63:0]      inf_res_int;

  reg   [3:0]       nan_flag_d1;
  reg   [63:0]      nan_res_d1;
  reg   [3:0]       inf_flag_d1;
  reg   [63:0]      inf_res_d1;

  InputProc u_InputProc(
    .prcn           (prcn),
    .op_A           (op_A),
    .op_B           (op_B),
    .op_C           (op_C),
    .mult_A         (mult_A),
    .mult_B         (mult_B),
    .align_C        (align_C),
    .sh_amt         (sh_amt_int),
    .subtract       (subtract_int),
    .sig_tmp        (sig_tmp_int),
    .exp_tmp        (exp_tmp_int)
  );

  AlignShifter u_AlignShifter(
    .prcn           (prcn),
    .op_C           (align_C),
    .sh_amt         (sh_amt_int),
    .subtract       (subtract_int),
    .addend         (addend_int),
    .sticky         (sticky_int)
  );

  Multiplier u_MultiPlier(
    .prcn           (prcn),
    .op_A           (mult_A),
    .op_B           (mult_B),
    .prod_sum       (prod_sum_int),
    .prod_carry     (prod_carry_int)
  );

  ExcepProc u_ExcepProc(
    .prcn           (prcn),
    .op_A           (op_A),
    .op_B           (op_B),
    .op_C           (op_C),
    .nan_flag       (nan_flag_int),
    .nan_res        (nan_res_int),
    .inf_flag       (inf_flag_int),
    .inf_res        (inf_res_int)
  );

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      vld_out     <= 1'd0;
      sh_amt      <= 27'd0;
      sh_amt_d1   <= 27'd0;
      sig_tmp     <= 4'd0;
      exp_tmp     <= 37'd0;
      exp_tmp_d1  <= 37'd0;
      addend      <= 163'd0;
      subtract      <= 3'd0;
      sticky      <= 4'd0;
      sticky_d1   <= 4'd0;
      prod_sum    <= 108'd0;
      prod_carry  <= 108'd0;
      nan_flag    <= 4'd0;
      nan_flag_d1 <= 4'd0;
      nan_res     <= 64'd0;
      nan_res_d1  <= 64'd0;
      inf_flag    <= 3'd0;
      inf_flag_d1 <= 3'd0;
      inf_res     <= 64'd0;
      inf_res_d1  <= 64'd0;
    end
    else begin
      vld_out     <= vld_in;
      sh_amt      <= sh_amt_d1;
      sh_amt_d1   <= sh_amt_int;
      sig_tmp     <= sig_tmp_int;
      exp_tmp     <= exp_tmp_d1;
      exp_tmp_d1  <= exp_tmp_int;
      addend      <= addend_int;
      subtract    <= subtract_int;
      sticky      <= sticky_d1;
      sticky_d1   <= sticky_int;
      prod_sum    <= prod_sum_int;
      prod_carry  <= prod_carry_int;
      nan_flag    <= nan_flag_d1;
      nan_flag_d1 <= nan_flag_int;
      nan_res     <= nan_res_d1;
      nan_res_d1  <= nan_res_int;
      inf_flag    <= inf_flag_d1;
      inf_flag_d1 <= inf_flag_int;
      inf_res     <= inf_res_d1;
      inf_res_d1  <= inf_res_int;
    end
  end

endmodule