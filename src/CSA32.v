module CSA32 #(
  parameter WIDTH = 13
)(
  input   [WIDTH-1:0]   op_A,
  input   [WIDTH-1:0]   op_B,
  input   [WIDTH-1:0]   op_C,
  output  [WIDTH-1:0]   sum,
  output  [WIDTH-1:0]   carry
);

  wire  [WIDTH-1:0]     P, G;

  assign G      = op_A & op_B;
  assign P      = op_A ^ op_B;
  assign sum    = P ^ op_C;
  assign carry  = G | (P & op_C);

endmodule