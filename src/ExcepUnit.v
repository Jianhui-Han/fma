module ExcepUnit #(
  parameter DATA_WIDTH  = 64,
  parameter EXP_WIDTH   = 11,
  parameter MANT_WIDTH  = 52
)(
  input   [DATA_WIDTH-1:0]    op_A,
  input   [DATA_WIDTH-1:0]    op_B,
  input   [DATA_WIDTH-1:0]    op_C,
  output                      nan_flag,
  output  [DATA_WIDTH-1:0]    nan_res,
  output                      inf_flag,
  output  [DATA_WIDTH-1:0]    inf_res
);

  wire      high_A_1, high_B_1, high_C_1;
  wire      high_A_0, high_B_0;
  wire      low_A_0, low_B_0, low_C_0;
  wire      inf_A, nan_A, zero_A, fin_A;
  wire      inf_B, nan_B, zero_B, fin_B;
  wire      inf_C, nan_C, fin_C;
  wire      inf_AB, nan_AB, fin_AB, sig_AB;
  
  wire  [DATA_WIDTH-1:0]      nan_res_AB;
  wire  [DATA_WIDTH-1:0]      nan_default;

  wire                        inf_sig;
  wire  [DATA_WIDTH-1:0]      mask;


  assign high_A_1 = op_A[DATA_WIDTH-2:DATA_WIDTH-EXP_WIDTH-1] == {{EXP_WIDTH}{1'b1}};
  assign high_A_0 = op_A[DATA_WIDTH-2:DATA_WIDTH-EXP_WIDTH-1] == {{EXP_WIDTH}{1'b0}};
  assign high_B_1 = op_B[DATA_WIDTH-2:DATA_WIDTH-EXP_WIDTH-1] == {{EXP_WIDTH}{1'b1}};
  assign high_B_0 = op_B[DATA_WIDTH-2:DATA_WIDTH-EXP_WIDTH-1] == {{EXP_WIDTH}{1'b0}};
  assign high_C_1 = op_C[DATA_WIDTH-2:DATA_WIDTH-EXP_WIDTH-1] == {{EXP_WIDTH}{1'b1}};
  assign low_A_0  = op_A[MANT_WIDTH-1:0] == {{MANT_WIDTH}{1'b0}};
  assign low_B_0  = op_B[MANT_WIDTH-1:0] == {{MANT_WIDTH}{1'b0}};
  assign low_C_0  = op_C[MANT_WIDTH-1:0] == {{MANT_WIDTH}{1'b0}};

  assign inf_A    = high_A_1 & low_A_0;
  assign nan_A    = high_A_1 & ~low_A_0;
  assign zero_A   = high_A_0 & low_A_0;
  assign fin_A    = ~high_A_1 & ~zero_A;
  assign inf_B    = high_B_1 & low_B_0;
  assign nan_B    = high_B_1 & ~low_B_0;
  assign zero_B   = high_B_0 & low_B_0;
  assign fin_B    = ~high_B_1 & ~zero_B;
  assign inf_C    = high_C_1 & low_C_0;
  assign nan_C    = high_C_1 & ~low_C_0;
  assign fin_C    = ~high_C_1;

  assign sig_AB   = op_A[DATA_WIDTH-1] ^ op_B[DATA_WIDTH-1];
  assign inf_AB   = (inf_A & inf_B) | (inf_A & fin_B) | (fin_A & inf_B);
  assign nan_AB   = nan_A | nan_B | (inf_A & zero_B) | (zero_A & inf_B);
  assign fin_AB   = ~inf_AB & ~nan_AB;
  
  assign nan_default  = {1'd1, {{EXP_WIDTH}{1'b1}}, 1'b1, {{MANT_WIDTH-1}{1'b0}}};
  assign nan_res_AB   = nan_A ? op_A : (nan_B ? op_B : (nan_AB ? nan_default : 'd0));

  assign inf_flag = ((sig_AB ^~ op_C[DATA_WIDTH-1]) & inf_AB & inf_C) | (inf_AB & fin_C) | (fin_AB & inf_C); 
  assign nan_flag = nan_AB | nan_C | ((sig_AB ^ op_C[DATA_WIDTH-1]) & inf_AB & inf_C);
  assign inf_sig  = inf_flag ? (inf_AB ? sig_AB : op_C[DATA_WIDTH-1]) : 1'd0;
  assign inf_res  = {inf_sig, {{EXP_WIDTH}{1'b1}}, {{MANT_WIDTH}{1'b0}}};
  assign mask     = {{{DATA_WIDTH-MANT_WIDTH}{1'b0}}, 1'b1, {{MANT_WIDTH-1}{1'b0}}};
  assign nan_res  = (nan_AB ? nan_res_AB : (nan_C ? op_C : (nan_flag ? nan_default : {{DATA_WIDTH}{1'b0}}))) | mask;
  
endmodule