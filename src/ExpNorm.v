module ExpNorm #(
  parameter EXP_WIDTH   = 13,
  parameter AMT_WIDTH   = 8
)(
  input   [EXP_WIDTH-1:0]     exp_tmp,
  input   [AMT_WIDTH-1:0]     sh_amt,
  input   [AMT_WIDTH-1:0]     sh_const,
  input   [EXP_WIDTH-1:0]     sh_amt_s1,
  input   [AMT_WIDTH-2:0]     lzac_cnt,
  input                       lzac_vld,
  output  [EXP_WIDTH-1:0]     exp_norm,
  output                      sh_en_s1,
  output  [AMT_WIDTH-2:0]     sh_amt_s2,
  input                       subtract,
  output                      sh_less_s1,
  input   [AMT_WIDTH-1:0]     amt_mask
);

  wire  [EXP_WIDTH-1:0]     csa_0_B, csa_0_C, csa_0_sum, csa_0_carry;
  wire  [EXP_WIDTH-1:0]     exp_norm_raw;
  wire  [AMT_WIDTH-1:0]     exp_tmp_s2, exp_tmp_s2_0;
  wire                      subnorm;

  CSA32 #(
    .WIDTH      (EXP_WIDTH)
  )
  u_CSA_0(
    .op_A       (exp_tmp),
    .op_B       (csa_0_B),
    .op_C       (csa_0_C),
    .sum        (csa_0_sum),
    .carry      (csa_0_carry)
  );

  // subtraction: exp_tmp - sh_amt_s1 - sh_amt_s2
  assign sh_en_s1     = (sh_amt >= sh_const+2) | (subtract & (sh_amt >= sh_const));
  assign csa_0_B      = sh_en_s1 ? sh_amt_s1 : 0; // already 2-complemented.
  assign csa_0_C      = sh_en_s1 ? (lzac_vld ? ~lzac_cnt : 0) : ~sh_amt;

  assign exp_norm_raw = {1'd0, csa_0_sum} + {csa_0_carry, 1'd1};    // add 1 for op_C's complement
  assign subnorm      = exp_norm_raw[EXP_WIDTH-1] | (exp_norm_raw == 0) | (sh_en_s1 & ~lzac_vld);
  assign exp_norm     = subnorm ? 0 : exp_norm_raw;
  
  assign exp_tmp_s2_0 = exp_tmp + sh_amt_s1;
  assign exp_tmp_s2   = ((exp_tmp_s2_0 >= amt_mask) | (exp_tmp_s2_0 == 0)) ? 0 : exp_tmp_s2_0-1;
  assign sh_amt_s2    = sh_en_s1 ? (subnorm ? exp_tmp_s2 : lzac_cnt) : (subnorm ? exp_tmp-1 : sh_amt);
  
  assign sh_less_s1   = sh_en_s1 & (exp_tmp_s2_0 == 0);

endmodule