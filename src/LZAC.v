module LZAC(
  input   [1:0]         prcn,
  input   [107:0]       sum,
  input   [107:0]       carry,
  input   [2:0]         concat,
  input   [3:0]         zero,
  output  [22:0]        count,
  output  [3:0]         valid
);

  wire    [4:0]     lzac_3_cnt, lzac_2_cnt, lzac_1_cnt, lzac_0_cnt;
  wire              lzac_3_vld, lzac_2_vld, lzac_1_vld, lzac_0_vld;

  wire    [5:0]     count_s_1, count_s_0;
  wire              valid_s_1, valid_s_0;
  wire    [6:0]     count_d_0;
  wire              valid_d_0;
  wire    [22:0]    count_d, count_s, count_h;
  wire    [3:0]     valid_d, valid_s, valid_h;

  LZAC32 #(
    .WIDTH        (27)
  )
  u_LZAC32_0(
    .sum          ({sum[26:0], 1'd0}),
    .carry        ({carry[26:0], 1'd0}),
    .count        (lzac_0_cnt),
    .valid        (lzac_0_vld)
  );

  LZAC32 #(
    .WIDTH        (27)
  ) u_LZAC32_1(
    .sum          (sum[53:26]),
    .carry        (carry[53:26]),
    .count        (lzac_1_cnt),
    .valid        (lzac_1_vld)
  );

  LZAC32 #(
    .WIDTH        (27)
  ) u_LZAC32_2(
    .sum          (sum[80:53]),
    .carry        (carry[80:53]),
    .count        (lzac_2_cnt),
    .valid        (lzac_2_vld)
  );

  LZAC32 #(
    .WIDTH        (27)
  ) u_LZAC32_3(
    .sum          (sum[107:80]),
    .carry        (carry[107:80]),
    .count        (lzac_3_cnt),
    .valid        (lzac_3_vld)
  );

  assign valid_s_0 = lzac_0_vld | zero[0] | lzac_1_vld | zero[1];
  assign count_s_0 = lzac_1_vld ? {1'd0, lzac_1_cnt} : (concat[0] ? 6'd27 : lzac_0_cnt+5'd27);
  assign valid_s_1 = lzac_2_vld | zero[2] | lzac_3_vld | zero[3];
  assign count_s_1 = lzac_3_vld ? {1'd0, lzac_3_cnt} : (concat[2] ? 6'd27 : lzac_2_cnt+5'd27);
  assign valid_d_0 = valid_s_0 | valid_s_1;
  assign count_d_0 = valid_s_1 ? {1'd0, count_s_1} : (concat[1] ? 7'd54 : count_s_0+6'd54);
  assign count_d   = zero[3] ? 22'd0 : {count_d_0, count_d_0[4:0], count_d_0[5:0], count_d_0[4:0]};
  assign count_s[22:11] = zero[3] ? 12'd0 : {1'd0, count_s_1, count_s_1[4:0]};
  assign count_s[10:0]  = zero[1] ? 11'd0 : {count_s_0, count_s_0[4:0]};
  assign count_h[22:16] = zero[3] ? 7'd0 : {2'd0, lzac_3_cnt};
  assign count_h[15:11] = zero[2] ? 5'd0 : lzac_2_cnt;
  assign count_h[10:5]  = zero[1] ? 6'd0 : {1'd0, lzac_1_cnt};
  assign count_h[4:0]   = zero[0] ? 5'd0 : lzac_0_cnt;
  assign count      = prcn[1] ? count_d : (prcn[0] ? count_s : count_h);
  assign valid_d    = {4{valid_d_0}};
  assign valid_s[3] = (valid_s_1 & ~(count_s_1>=6'd50)) | zero[3];
  assign valid_s[2] = valid_s[3];
  assign valid_s[1] = (valid_s_0 & ~(count_s_0>=6'd50)) | zero[1];
  assign valid_s[0] = valid_s[1];
  assign valid_h[3] = (lzac_3_vld & ~(lzac_3_cnt>=5'd24)) | zero[3]; 
  assign valid_h[2] = (lzac_2_vld & ~(lzac_2_cnt>=5'd24)) | zero[2]; 
  assign valid_h[1] = (lzac_1_vld & ~(lzac_1_cnt>=5'd24)) | zero[1]; 
  assign valid_h[0] = (lzac_0_vld & ~(lzac_0_cnt>=5'd24)) | zero[0];
  assign valid      = prcn[1] ? valid_d : (prcn[0] ? valid_s : valid_h);

endmodule