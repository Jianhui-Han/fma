module FMAS2(
  input                 clk,
  input                 rst_n,
  input       [1:0]     prcn,
  input       [2:0]     rnd_mode,
  input                 vld_in,
  input       [107:0]   prod_sum,
  input       [107:0]   prod_carry,
  input       [162:0]   addend,
  input       [3:0]     subtract,
  input       [3:0]     sig_tmp,
  output reg            vld_out,
  output reg  [162:0]   sum_comp,
  output reg  [3:0]     sig_fma,
  output reg  [22:0]    lzac_cnt,
  output reg  [3:0]     lzac_vld,
  output reg  [3:0]     subt_out
);

  wire    [162:0]   sum_comp_int;
  wire    [3:0]     sig_fma_int;
  wire    [22:0]    lzac_cnt_int;
  wire    [3:0]     lzac_vld_int;

  Adder u_Adder(
    .prcn           (prcn),
    .rnd_mode       (rnd_mode),
    .prod_sum       (prod_sum),
    .prod_carry     (prod_carry),
    .addend         (addend),
    .subtract       (subtract),
    .sig_tmp        (sig_tmp),
    .sum_comp       (sum_comp_int),
    .sig_fma        (sig_fma_int),
    .lzac_cnt       (lzac_cnt_int),
    .lzac_vld       (lzac_vld_int)
  );

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      vld_out   <= 1'd0;
      sum_comp  <= 163'd0;
      sig_fma   <= 4'd0;
      lzac_cnt  <= 23'd0;
      lzac_vld  <= 4'd0;
      subt_out  <= 4'd0;
    end
    else begin
      vld_out   <= vld_in;
      sum_comp  <= sum_comp_int;
      sig_fma   <= sig_fma_int;
      lzac_cnt  <= lzac_cnt_int;
      lzac_vld  <= lzac_vld_int;
      subt_out  <= subtract;
    end
  end

endmodule