module LDCT32#(
  parameter WIDTH = 32
)(
  input   [WIDTH-1:0]     ind,
  output  [4:0]           count,
  output                  valid
);

  wire  [31:0]            ind_0;
  wire  [15:0]            count_0;
  wire  [15:0]            cnt_0_low;
  wire  [7:0]             valid_0;
  wire  [11:0]            count_1;
  wire  [3:0]             valid_1;
  wire  [7:0]             count_2;
  wire  [1:0]             valid_2;

  assign ind_0 = {ind, {{32-WIDTH}{1'b1}}};

  genvar i;
  generate
    for (i=0; i<8; i=i+1) begin : gen_level_0
      assign valid_0[i]           = |ind_0[i*4+3:i*4];
      assign cnt_0_low[i*2+1:i*2] = ind_0[i*4+1] ? 2'd2 : (ind_0[i*4] ? 2'd3 : 2'd0);
      assign count_0[i*2+1:i*2]   = ind_0[i*4+3] ? 2'd0 : (ind_0[i*4+2] ? 2'd1 : cnt_0_low[i*2+1:i*2]);
    end

    for (i=0; i<4; i=i+1) begin : gen_level_1
      assign valid_1[i]         = valid_0[i*2+1] | valid_0[i*2];
      assign count_1[i*3+2:i*3] = valid_0[i*2+1] ? {1'd0, count_0[i*4+3:i*4+2]} : count_0[i*4+1:i*4]+4;
    end

    for (i=0; i<2; i=i+1) begin : gen_level_2
      assign valid_2[i]         = valid_1[i*2+1] | valid_1[i*2];
      assign count_2[i*4+3:i*4] = valid_1[i*2+1] ? {1'd0, count_1[i*6+5:i*6+3]} : count_1[i*6+2:i*6]+8; 
    end
  endgenerate

  assign valid = |ind;
  assign count = valid_2[1] ? {1'd0, count_2[7:4]} : count_2[3:0]+16;

endmodule