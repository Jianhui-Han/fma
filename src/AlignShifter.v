module AlignShifter(
  input   [1:0]       prcn,
  input   [162:0]     op_C,
  input   [26:0]      sh_amt,
  input   [3:0]       subtract,
  output  [162:0]     addend,
  output  [3:0]       sticky
);

  wire  [39:0]    data_in_0_0, data_out_0_0, data_sh_0_0;
  wire  [40:0]    data_in_0_1, data_out_0_1, data_sh_0_1;
  wire  [39:0]    data_in_0_2, data_out_0_2, data_sh_0_2;
  wire  [41:0]    data_in_0_3, data_out_0_3, data_sh_0_3;
  wire            shift_in_0_0, shift_in_0_1, shift_in_0_2;
  wire  [39:0]    data_out_1_0, data_sh_1_0;
  wire  [40:0]    data_out_1_1, data_sh_1_1;
  wire  [39:0]    data_out_1_2, data_sh_1_2;
  wire  [41:0]    data_out_1_3, data_sh_1_3;
  wire  [1:0]     shift_in_1_0, shift_in_1_1, shift_in_1_2;
  wire  [39:0]    data_out_2_0, data_sh_2_0;
  wire  [40:0]    data_out_2_1, data_sh_2_1;
  wire  [39:0]    data_out_2_2, data_sh_2_2;
  wire  [41:0]    data_out_2_3, data_sh_2_3;
  wire  [3:0]     shift_in_2_0, shift_in_2_1, shift_in_2_2;
  wire  [39:0]    data_out_3_0, data_sh_3_0;
  wire  [40:0]    data_out_3_1, data_sh_3_1;
  wire  [39:0]    data_out_3_2, data_sh_3_2;
  wire  [41:0]    data_out_3_3, data_sh_3_3;
  wire  [7:0]     shift_in_3_0, shift_in_3_1, shift_in_3_2;
  wire  [39:0]    data_out_4_0, data_sh_4_0;
  wire  [40:0]    data_out_4_1, data_sh_4_1;
  wire  [39:0]    data_out_4_2, data_sh_4_2;
  wire  [41:0]    data_out_4_3, data_sh_4_3;
  wire  [15:0]    shift_in_4_0, shift_in_4_1, shift_in_4_2;
  wire  [39:0]    data_out_5_0, data_sh_5_0;
  wire  [40:0]    data_out_5_1, data_sh_5_1;
  wire  [39:0]    data_out_5_2, data_sh_5_2;
  wire  [41:0]    data_out_5_3, data_sh_5_3;
  wire  [31:0]    shift_in_5_0, shift_in_5_1, shift_in_5_2;
  wire  [80:0]    data_in_6_0, data_out_6_0, data_sh_6_0;
  wire  [81:0]    data_in_6_1, data_out_6_1, data_sh_6_1;
  wire  [63:0]    shift_in_6_0;
  wire  [162:0]   data_in_7_0, data_out_7_0, data_sh_7_0;
  wire            subtract_3, subtract_2, subtract_1, subtract_0;

  // level 0
  assign data_in_0_0  = op_C[39:0];
  assign shift_in_0_0 = (prcn == 2'b00) ? 1'd0 : data_in_0_1[0];
  assign data_sh_0_0  = {shift_in_0_0, data_in_0_0[39:1]};
  assign data_out_0_0 = sh_amt[0] ? data_sh_0_0 : data_in_0_0;
  assign data_in_0_1  = op_C[80:40];
  assign shift_in_0_1 = prcn[1] ? data_in_0_2[0] : 1'd0;
  assign data_sh_0_1  = {shift_in_0_1, data_in_0_1[40:1]};
  assign data_out_0_1 = sh_amt[6] ? data_sh_0_1 : data_in_0_1;
  assign data_in_0_2  = op_C[120:81];
  assign shift_in_0_2 = (prcn == 2'b00) ? 1'b0 : data_in_0_3[0];
  assign data_sh_0_2  = {shift_in_0_2, data_in_0_2[39:1]};
  assign data_out_0_2 = sh_amt[13] ? data_sh_0_2 : data_in_0_2;
  assign data_in_0_3  = op_C[162:121];
  assign data_sh_0_3  = {1'd0, data_in_0_3[41:1]};
  assign data_out_0_3 = sh_amt[19] ? data_sh_0_3 : data_in_0_3;

  assign sticky_0_0 = sh_amt[0] ? data_in_0_0[0] : 1'd0;
  assign sticky_0_1 = sh_amt[6] ? data_in_0_1[0] : 1'd0;
  assign sticky_0_2 = sh_amt[13] ? data_in_0_2[0] : 1'd0;
  assign sticky_0_3 = sh_amt[19] ? data_in_0_3[0] : 1'd0;

  // level 1
  assign shift_in_1_0 = (prcn == 2'b00) ? 2'd0 : data_out_0_1[1:0];
  assign data_sh_1_0  = {shift_in_1_0, data_out_0_0[39:2]};
  assign data_out_1_0 = sh_amt[1] ? data_sh_1_0 : data_out_0_0;
  assign shift_in_1_1 = prcn[1] ? data_out_0_2[1:0] : 2'd0;
  assign data_sh_1_1  = {shift_in_1_1, data_out_0_1[40:2]};
  assign data_out_1_1 = sh_amt[7] ? data_sh_1_1 : data_out_0_1;
  assign shift_in_1_2 = (prcn == 2'b00) ? 2'd0 : data_out_0_3[1:0];
  assign data_sh_1_2  = {shift_in_1_2, data_out_0_2[39:2]};
  assign data_out_1_2 = sh_amt[14] ? data_sh_1_2 : data_out_0_2;
  assign data_sh_1_3  = {2'd0, data_out_0_3[41:2]};
  assign data_out_1_3 = sh_amt[20] ? data_sh_1_3 : data_out_0_3;

  assign sticky_1_0 = sh_amt[1] ? (|data_out_0_0[1:0]) : 1'd0;
  assign sticky_1_1 = sh_amt[7] ? (|data_out_0_1[1:0]) : 1'd0;
  assign sticky_1_2 = sh_amt[14] ? (|data_out_0_2[1:0]) : 1'd0;
  assign sticky_1_3 = sh_amt[20] ? (|data_out_0_3[1:0]) : 1'd0;

  // level 2
  assign shift_in_2_0 = (prcn == 2'b00) ? 4'd0 : data_out_1_1[3:0];
  assign data_sh_2_0  = {shift_in_2_0, data_out_1_0[39:4]};
  assign data_out_2_0 = sh_amt[2] ? data_sh_2_0 : data_out_1_0;
  assign shift_in_2_1 = prcn[1] ? data_out_1_2[3:0] : 4'd0;
  assign data_sh_2_1  = {shift_in_2_1, data_out_1_1[40:4]};
  assign data_out_2_1 = sh_amt[8] ? data_sh_2_1 : data_out_1_1;
  assign shift_in_2_2 = (prcn == 2'b00) ? 4'd0 : data_out_1_3[3:0];
  assign data_sh_2_2  = {shift_in_2_2, data_out_1_2[39:4]};
  assign data_out_2_2 = sh_amt[15] ? data_sh_2_2 : data_out_1_2;
  assign data_sh_2_3  = {4'd0, data_out_1_3[41:4]};
  assign data_out_2_3 = sh_amt[21] ? data_sh_2_3 : data_out_1_3;

  assign sticky_2_0 = sh_amt[2] ? (|data_out_1_0[3:0]) : 1'd0;
  assign sticky_2_1 = sh_amt[8] ? (|data_out_1_1[3:0]) : 1'd0;
  assign sticky_2_2 = sh_amt[15] ? (|data_out_1_2[3:0]) : 1'd0;
  assign sticky_2_3 = sh_amt[21] ? (|data_out_1_3[3:0]) : 1'd0;

  // level 3
  assign shift_in_3_0 = (prcn == 2'b00) ? 8'd0 : data_out_2_1[7:0];
  assign data_sh_3_0  = {shift_in_3_0, data_out_2_0[39:8]};
  assign data_out_3_0 = sh_amt[3] ? data_sh_3_0 : data_out_2_0;
  assign shift_in_3_1 = prcn[1] ? data_out_2_2[7:0] : 8'd0;
  assign data_sh_3_1  = {shift_in_3_1, data_out_2_1[40:8]};
  assign data_out_3_1 = sh_amt[9] ? data_sh_3_1 : data_out_2_1;
  assign shift_in_3_2 = (prcn == 2'b00) ? 8'd0 : data_out_2_3[7:0];
  assign data_sh_3_2  = {shift_in_3_2, data_out_2_2[39:8]};
  assign data_out_3_2 = sh_amt[16] ? data_sh_3_2 : data_out_2_2;
  assign data_sh_3_3  = {8'd0, data_out_2_3[41:8]};
  assign data_out_3_3 = sh_amt[22] ? data_sh_3_3 : data_out_2_3;

  assign sticky_3_0 = sh_amt[3] ? (|data_out_2_0[7:0]) : 1'd0;
  assign sticky_3_1 = sh_amt[9] ? (|data_out_2_1[7:0]) : 1'd0;
  assign sticky_3_2 = sh_amt[16] ? (|data_out_2_2[7:0]) : 1'd0;
  assign sticky_3_3 = sh_amt[22] ? (|data_out_2_3[7:0]) : 1'd0;

  // level 4
  assign shift_in_4_0 = (prcn == 2'b00) ? 16'd0 : data_out_3_1[15:0];
  assign data_sh_4_0  = {shift_in_4_0, data_out_3_0[39:16]};
  assign data_out_4_0 = sh_amt[4] ? data_sh_4_0 : data_out_3_0;
  assign shift_in_4_1 = prcn[1] ? data_out_3_2[15:0] : 16'd0;
  assign data_sh_4_1  = {shift_in_4_1, data_out_3_1[40:16]};
  assign data_out_4_1 = sh_amt[10] ? data_sh_4_1 : data_out_3_1;
  assign shift_in_4_2 = (prcn == 2'b00) ? 16'd0 : data_out_3_3[15:0];
  assign data_sh_4_2  = {shift_in_4_2, data_out_3_2[39:16]};
  assign data_out_4_2 = sh_amt[17] ? data_sh_4_2 : data_out_3_2;
  assign data_sh_4_3  = {16'd0, data_out_3_3[41:16]};
  assign data_out_4_3 = sh_amt[23] ? data_sh_4_3 : data_out_3_3;

  assign sticky_4_0 = sh_amt[4] ? (|data_out_3_0[15:0]) : 1'd0;
  assign sticky_4_1 = sh_amt[10] ? (|data_out_3_1[15:0]) : 1'd0;
  assign sticky_4_2 = sh_amt[17] ? (|data_out_3_2[15:0]) : 1'd0;
  assign sticky_4_3 = sh_amt[23] ? (|data_out_3_3[15:0]) : 1'd0;

  // level 5
  assign shift_in_5_0 = (prcn == 2'b00) ? 32'd0 : data_out_4_1[31:0];
  assign data_sh_5_0  = {shift_in_5_0, data_out_4_0[39:32]};
  assign data_out_5_0 = sh_amt[5] ? data_sh_5_0 : data_out_4_0;
  assign shift_in_5_1 = prcn[1] ? data_out_4_2[31:0] : 32'd0;
  assign data_sh_5_1  = {shift_in_5_1, data_out_4_1[40:32]};
  assign data_out_5_1 = sh_amt[11] ? data_sh_5_1 : data_out_4_1;
  assign shift_in_5_2 = (prcn == 2'b00) ? 32'd0 : data_out_4_3[31:0];
  assign data_sh_5_2  = {shift_in_5_2, data_out_4_2[39:32]};
  assign data_out_5_2 = sh_amt[18] ? data_sh_5_2 : data_out_4_2;
  assign data_sh_5_3  = {32'd0, data_out_4_3[41:32]};
  assign data_out_5_3 = sh_amt[24] ? data_sh_5_3 : data_out_4_3;

  assign sticky_5_0 = sh_amt[5] ? (|data_out_4_0[31:0]) : 1'd0;
  assign sticky_5_1 = sh_amt[11] ? (|data_out_4_1[31:0]) : 1'd0;
  assign sticky_5_2 = sh_amt[18] ? (|data_out_4_2[31:0]) : 1'd0;
  assign sticky_5_3 = sh_amt[24] ? (|data_out_4_3[31:0]) : 1'd0;

  // level 6
  assign data_in_6_0  = {data_out_5_1, data_out_5_0};
  assign shift_in_6_0 = prcn[1] ? data_in_6_1[63:0] : 64'd0;
  assign data_sh_6_0  = {shift_in_6_0, data_in_6_0[80:64]};
  assign data_out_6_0 = sh_amt[12] ? data_sh_6_0 : data_in_6_0;
  assign data_in_6_1  = {data_out_5_3, data_out_5_2};
  assign data_sh_6_1  = {64'd0, data_in_6_1[81:64]};
  assign data_out_6_1 = sh_amt[25] ? data_sh_6_1 : data_in_6_1;

  assign sticky_6_0 = sh_amt[12] ? (|data_in_6_0[63:0]) : 1'd0;
  assign sticky_6_1 = sh_amt[25] ? (|data_in_6_1[63:0]) : 1'd0;

  // level 7
  assign data_in_7_0  = {data_out_6_1, data_out_6_0};
  assign data_sh_7_0  = {128'd0, data_in_7_0[162:128]};
  assign data_out_7_0 = sh_amt[26] ? data_sh_7_0 : data_in_7_0;

  assign sticky_7_0 = sh_amt[26] ? (|data_in_7_0[127:0]) : 1'd0;

  // subtract operand C
  assign subtract_3 = subtract[3];
  assign subtract_2 = (prcn == 2'b00) ? subtract[2] : subtract[3];
  assign subtract_1 = prcn[1] ? subtract[3] : subtract[1];
  assign subtract_0 = prcn[1] ? subtract[3] : (prcn[0] ? subtract[1] : subtract[0]);
  assign addend[162:121]  = subtract_3 ? ~data_out_7_0[162:121] : data_out_7_0[162:121];
  assign addend[120:81]   = subtract_2 ? ~data_out_7_0[120:81] : data_out_7_0[120:81];
  assign addend[80:40]    = subtract_1 ? ~data_out_7_0[80:40] : data_out_7_0[80:40];
  assign addend[39:0]     = subtract_0 ? ~data_out_7_0[39:0] : data_out_7_0[39:0];

  // sticky bits
  assign sticky[3] = sticky_0_3 | sticky_1_3 | sticky_2_3 | sticky_3_3 | sticky_4_3 | sticky_5_3;
  assign sticky[2] = sticky_0_2 | sticky_1_2 | sticky_2_2 | sticky_3_2 | sticky_4_2 | sticky_5_2 | sticky_6_1;
  assign sticky[1] = sticky_0_1 | sticky_1_1 | sticky_2_1 | sticky_3_1 | sticky_4_1 | sticky_5_1;
  assign sticky[0] = sticky_0_0 | sticky_1_0 | sticky_2_0 | sticky_3_0 | sticky_4_0 | sticky_5_0 | sticky_6_0 | sticky_7_0;

endmodule