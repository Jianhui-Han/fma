module InputProc(
  input   [1:0]         prcn,
  input   [63:0]        op_A,
  input   [63:0]        op_B,
  input   [63:0]        op_C,
  output  [59:0]        mult_A,
  output  [59:0]        mult_B,
  output  [162:0]       align_C,
  output  [26:0]        sh_amt,  
  output  [3:0]         subtract,
  output  [3:0]         sig_tmp,
  output  [36:0]        exp_tmp
);

  wire              subnorm_A_d_0, subnorm_B_d_0, subnorm_C_d_0;
  wire              subnorm_A_s_1, subnorm_B_s_1, subnorm_C_s_1;
  wire              subnorm_A_s_0, subnorm_B_s_0, subnorm_C_s_0;
  wire              subnorm_A_h_3, subnorm_B_h_3, subnorm_C_h_3;
  wire              subnorm_A_h_2, subnorm_B_h_2, subnorm_C_h_2;
  wire              subnorm_A_h_1, subnorm_B_h_1, subnorm_C_h_1;
  wire              subnorm_A_h_0, subnorm_B_h_0, subnorm_C_h_0;

  wire              zero_A_d_0, zero_B_d_0, zero_C_d_0;
  wire              zero_A_s_1, zero_B_s_1, zero_C_s_1;
  wire              zero_A_s_0, zero_B_s_0, zero_C_s_0;
  wire              zero_A_h_3, zero_B_h_3, zero_C_h_3;
  wire              zero_A_h_2, zero_B_h_2, zero_C_h_2;
  wire              zero_A_h_1, zero_B_h_1, zero_C_h_1;
  wire              zero_A_h_0, zero_B_h_0, zero_C_h_0;
  wire  [3:0]       zero_A, zero_B, zero_C;

  wire              impl_A_d_0, impl_B_d_0, impl_C_d_0;
  wire              impl_A_s_1, impl_B_s_1, impl_C_s_1;
  wire              impl_A_s_0, impl_B_s_0, impl_C_s_0;
  wire              impl_A_h_3, impl_B_h_3, impl_C_h_3;
  wire              impl_A_h_2, impl_B_h_2, impl_C_h_2;
  wire              impl_A_h_1, impl_B_h_1, impl_C_h_1;
  wire              impl_A_h_0, impl_B_h_0, impl_C_h_0;

  wire  [12:0]      exp_proc_3_A, exp_proc_3_B, exp_proc_3_C;
  wire  [6:0]       exp_proc_2_A, exp_proc_2_B, exp_proc_2_C;
  wire  [9:0]       exp_proc_1_A, exp_proc_1_B, exp_proc_1_C;
  wire  [6:0]       exp_proc_0_A, exp_proc_0_B, exp_proc_0_C;
  wire  [12:0]      exp_proc_3_bias, exp_tmp_3, sh_const_3;
  wire  [6:0]       exp_proc_2_bias, exp_tmp_2, sh_const_2;
  wire  [9:0]       exp_proc_1_bias, exp_tmp_1, sh_const_1;
  wire  [6:0]       exp_proc_0_bias, exp_tmp_0, sh_const_0;
  wire  [7:0]       sh_max_3, sh_amt_3;
  wire  [5:0]       sh_max_2, sh_amt_2;
  wire  [6:0]       sh_max_1, sh_amt_1;
  wire  [5:0]       sh_max_0, sh_amt_0;

  wire  [59:0]      mult_A_d, mult_A_s, mult_A_h;
  wire  [59:0]      mult_B_d, mult_B_s, mult_B_h;
  wire  [162:0]     align_C_d, align_C_s, align_C_h;

  wire  [26:0]      sh_amt_d, sh_amt_s, sh_amt_h;

  ExpProc #(
    .EXP_WIDTH    (13),
    .AMT_WIDTH    (8)
  )
  u_ExpProc_3(
    .exp_A        (exp_proc_3_A),
    .exp_B        (exp_proc_3_B),
    .exp_C        (exp_proc_3_C),
    .sh_const     (sh_const_3),
    .sh_max       (sh_max_3),
    .exp_tmp      (exp_tmp_3),
    .sh_amt       (sh_amt_3),
    .zero_AB      (zero_A[3]|zero_B[3])
  );

  ExpProc #(
    .EXP_WIDTH    (7),
    .AMT_WIDTH    (6)
  )
  u_ExpProc_2(
    .exp_A        (exp_proc_2_A),
    .exp_B        (exp_proc_2_B),
    .exp_C        (exp_proc_2_C),
    .sh_const     (sh_const_2),
    .sh_max       (sh_max_2),
    .exp_tmp      (exp_tmp_2),
    .sh_amt       (sh_amt_2),
    .zero_AB      (zero_A[2]|zero_B[2])
  );

  ExpProc #(
    .EXP_WIDTH    (10),
    .AMT_WIDTH    (7)
  )
  u_ExpProc_1(
    .exp_A        (exp_proc_1_A),
    .exp_B        (exp_proc_1_B),
    .exp_C        (exp_proc_1_C),
    .sh_const     (sh_const_1),
    .sh_max       (sh_max_1),
    .exp_tmp      (exp_tmp_1),
    .sh_amt       (sh_amt_1),
    .zero_AB      (zero_A[1]|zero_B[1])
  );

  ExpProc #(
    .EXP_WIDTH    (7),
    .AMT_WIDTH    (6)
  )
  u_ExpProc_0(
    .exp_A        (exp_proc_0_A),
    .exp_B        (exp_proc_0_B),
    .exp_C        (exp_proc_0_C),
    .sh_const     (sh_const_0),
    .sh_max       (sh_max_0),
    .exp_tmp      (exp_tmp_0),
    .sh_amt       (sh_amt_0),
    .zero_AB      (zero_A[0]|zero_B[0])
  );
  
  // subnormal
  assign subnorm_A_d_0 = op_A[62:52] == 11'd0;
  assign subnorm_B_d_0 = op_B[62:52] == 11'd0;
  assign subnorm_C_d_0 = op_C[62:52] == 11'd0;
  assign subnorm_A_s_1 = op_A[62:55] == 8'd0;
  assign subnorm_A_s_0 = op_A[30:23] == 8'd0;
  assign subnorm_B_s_1 = op_B[62:55] == 8'd0;
  assign subnorm_B_s_0 = op_B[30:23] == 8'd0;
  assign subnorm_C_s_1 = op_C[62:55] == 8'd0;
  assign subnorm_C_s_0 = op_C[30:23] == 8'd0;
  assign subnorm_A_h_3 = op_A[62:58] == 5'd0;
  assign subnorm_A_h_2 = op_A[46:42] == 5'd0;
  assign subnorm_A_h_1 = op_A[30:26] == 5'd0;
  assign subnorm_A_h_0 = op_A[14:10] == 5'd0;
  assign subnorm_B_h_3 = op_B[62:58] == 5'd0;
  assign subnorm_B_h_2 = op_B[46:42] == 5'd0;
  assign subnorm_B_h_1 = op_B[30:26] == 5'd0;
  assign subnorm_B_h_0 = op_B[14:10] == 5'd0;
  assign subnorm_C_h_3 = op_C[62:58] == 5'd0;
  assign subnorm_C_h_2 = op_C[46:42] == 5'd0;
  assign subnorm_C_h_1 = op_C[30:26] == 5'd0;
  assign subnorm_C_h_0 = op_C[14:10] == 5'd0;

  assign zero_A_d_0 = subnorm_A_d_0 & (op_A[51:0] == 52'd0);
  assign zero_B_d_0 = subnorm_B_d_0 & (op_B[51:0] == 52'd0);
  assign zero_C_d_0 = subnorm_C_d_0 & (op_C[51:0] == 52'd0);
  assign zero_A_s_1 = subnorm_A_s_1 & (op_A[54:32] == 23'd0);
  assign zero_A_s_0 = subnorm_A_s_0 & (op_A[22:0] == 23'd0);
  assign zero_B_s_1 = subnorm_B_s_1 & (op_B[54:32] == 23'd0);
  assign zero_B_s_0 = subnorm_B_s_0 & (op_B[22:0] == 23'd0);
  assign zero_C_s_1 = subnorm_C_s_1 & (op_C[54:32] == 23'd0);
  assign zero_C_s_0 = subnorm_C_s_0 & (op_C[22:0] == 23'd0);
  assign zero_A_h_3 = subnorm_A_h_3 & (op_A[57:48] == 10'd0);
  assign zero_A_h_2 = subnorm_A_h_2 & (op_A[41:32] == 10'd0);
  assign zero_A_h_1 = subnorm_A_h_1 & (op_A[25:16] == 10'd0);
  assign zero_A_h_0 = subnorm_A_h_0 & (op_A[9:0] == 10'd0);
  assign zero_B_h_3 = subnorm_B_h_3 & (op_B[57:48] == 10'd0);
  assign zero_B_h_2 = subnorm_B_h_2 & (op_B[41:32] == 10'd0);
  assign zero_B_h_1 = subnorm_B_h_1 & (op_B[25:16] == 10'd0);
  assign zero_B_h_0 = subnorm_B_h_0 & (op_B[9:0] == 10'd0);
  assign zero_C_h_3 = subnorm_C_h_3 & (op_C[57:48] == 10'd0);
  assign zero_C_h_2 = subnorm_C_h_2 & (op_C[41:32] == 10'd0);
  assign zero_C_h_1 = subnorm_C_h_1 & (op_C[25:16] == 10'd0);
  assign zero_C_h_0 = subnorm_C_h_0 & (op_C[9:0] == 10'd0);
  assign zero_A[3]  = prcn[1] ? zero_A_d_0 : (prcn[0] ? zero_A_s_1 : zero_A_h_3);
  assign zero_A[2]  = zero_A_h_2;
  assign zero_A[1]  = prcn[0] ? zero_A_s_0 : zero_A_h_1;
  assign zero_A[0]  = zero_A_h_0;
  assign zero_B[3]  = prcn[1] ? zero_B_d_0 : (prcn[0] ? zero_B_s_1 : zero_B_h_3);
  assign zero_B[2]  = zero_B_h_2;
  assign zero_B[1]  = prcn[0] ? zero_B_s_0 : zero_B_h_1;
  assign zero_B[0]  = zero_B_h_0;
  assign zero_C[3]  = prcn[1] ? zero_C_d_0 : (prcn[0] ? zero_C_s_1 : zero_C_h_3);
  assign zero_C[2]  = zero_C_h_2;
  assign zero_C[1]  = prcn[0] ? zero_C_s_0 : zero_C_h_1;
  assign zero_C[0]  = zero_C_h_0;

  // implicit bit
  assign impl_A_d_0 = ~subnorm_A_d_0;
  assign impl_B_d_0 = ~subnorm_B_d_0;
  assign impl_C_d_0 = ~subnorm_C_d_0;
  assign impl_A_s_1 = ~subnorm_A_s_1;
  assign impl_A_s_0 = ~subnorm_A_s_0;
  assign impl_B_s_1 = ~subnorm_B_s_1;
  assign impl_B_s_0 = ~subnorm_B_s_0;
  assign impl_C_s_1 = ~subnorm_C_s_1;
  assign impl_C_s_0 = ~subnorm_C_s_0;
  assign impl_A_h_3 = ~subnorm_A_h_3;
  assign impl_A_h_2 = ~subnorm_A_h_2;
  assign impl_A_h_1 = ~subnorm_A_h_1;
  assign impl_A_h_0 = ~subnorm_A_h_0;
  assign impl_B_h_3 = ~subnorm_B_h_3;
  assign impl_B_h_2 = ~subnorm_B_h_2;
  assign impl_B_h_1 = ~subnorm_B_h_1;
  assign impl_B_h_0 = ~subnorm_B_h_0;
  assign impl_C_h_3 = ~subnorm_C_h_3;
  assign impl_C_h_2 = ~subnorm_C_h_2;
  assign impl_C_h_1 = ~subnorm_C_h_1;
  assign impl_C_h_0 = ~subnorm_C_h_0;

  // exponent processing
  assign exp_proc_3_A = prcn[1] ? {2'd0, op_A[62:52]} : (prcn[0] ? {5'd0, op_A[62:55]} : {8'd0, op_A[62:58]});
  assign exp_proc_3_B = prcn[1] ? {2'd0, op_B[62:52]} : (prcn[0] ? {5'd0, op_B[62:55]} : {8'd0, op_B[62:58]});
  assign exp_proc_3_C = prcn[1] ? {2'd0, op_C[62:52]} : (prcn[0] ? {5'd0, op_C[62:55]} : {8'd0, op_C[62:58]});
  assign exp_proc_2_A = {2'd0, op_A[46:42]};
  assign exp_proc_2_B = {2'd0, op_B[46:42]};
  assign exp_proc_2_C = {2'd0, op_C[46:42]};
  assign exp_proc_1_A = prcn[0] ? {2'd0, op_A[30:23]} : {5'd0, op_A[30:26]};
  assign exp_proc_1_B = prcn[0] ? {2'd0, op_B[30:23]} : {5'd0, op_B[30:26]};
  assign exp_proc_1_C = prcn[0] ? {2'd0, op_C[30:23]} : {5'd0, op_C[30:26]};
  assign exp_proc_0_A = {2'd0, op_A[14:10]};
  assign exp_proc_0_B = {2'd0, op_B[14:10]};
  assign exp_proc_0_C = {2'd0, op_C[14:10]};

  assign sh_const_3 = prcn[1] ? 13'd7225 : (prcn[0] ? 13'd8092 : 13'd8191);
  assign sh_const_2 = 7'd127;
  assign sh_const_1 = prcn[0] ? 10'd924 : 10'd1023;
  assign sh_const_0 = 7'd127;
  assign sh_max_3   = prcn[1] ? 8'd163 : (prcn[0] ? 8'd76 : 8'd37);
  assign sh_max_2   = 6'd37;
  assign sh_max_1   = prcn[0] ? 7'd76 : 7'd37;
  assign sh_max_0   = 6'd37;

  // multiplier operands
  assign mult_A_d  = {7'd0, impl_A_d_0, op_A[51:0]};
  assign mult_A_s  = {6'd0, impl_A_s_1, op_A[54:32], 6'd0, impl_A_s_0, op_A[22:0]};
  assign mult_A_h  = {4'd0, impl_A_h_3, op_A[57:48], 4'd0, impl_A_h_2, op_A[41:32],4'd0, impl_A_h_1, op_A[25:16], 4'd0, impl_A_h_0, op_A[9:0]};
  assign mult_A    = prcn[1] ? mult_A_d : (prcn[0] ? mult_A_s : mult_A_h);
  assign mult_B_d  = {7'd0, impl_B_d_0, op_B[51:0]};
  assign mult_B_s  = {6'd0, impl_B_s_1, op_B[54:32], 6'd0, impl_B_s_0, op_B[22:0]};
  assign mult_B_h  = {4'd0, impl_B_h_3, op_B[57:48], 4'd0, impl_B_h_2, op_B[41:32],4'd0, impl_B_h_1, op_B[25:16], 4'd0, impl_B_h_0, op_B[9:0]};
  assign mult_B    = prcn[1] ? mult_B_d : (prcn[0] ? mult_B_s : mult_B_h);

  // alignment shifter operand
  assign align_C_d = {impl_C_d_0, op_C[51:0], 110'd0};
  assign align_C_s = {6'd0, impl_C_s_1, op_C[54:32], 57'd0, impl_C_s_0, op_C[22:0], 52'd0};
  assign align_C_h = {5'd0, impl_C_h_3, op_C[57:48], 29'd0, impl_C_h_2, op_C[41:32], 30'd0, impl_C_h_1, op_C[25:16], 29'd0, impl_C_h_0, op_C[9:0], 26'd0};
  assign align_C   = prcn[1] ? align_C_d : (prcn[0] ? align_C_s : align_C_h);

  // shift amount and subtract flag
  assign sh_amt_d = {sh_amt_3, sh_amt_3[5:0], sh_amt_3[6:0], sh_amt_3[5:0]}; 
  assign sh_amt_s = {sh_amt_3, sh_amt_3[5:0], sh_amt_1, sh_amt_1[5:0]};
  assign sh_amt_h = {sh_amt_3, sh_amt_2, sh_amt_1, sh_amt_0};
  assign sh_amt   = prcn[1] ? sh_amt_d : (prcn[0] ? sh_amt_s : sh_amt_h);
  assign subtract = {sig_tmp[3]^op_C[63], sig_tmp[2]^op_C[47], sig_tmp[1]^op_C[31], sig_tmp[0]^op_C[15]};

  // sign and exponent
  assign sig_tmp[3] = (zero_A[3] | zero_B[3]) ? ((op_A[63] ^ op_B[63]) & (op_C[63] & zero_C[3])) : (op_A[63] ^ op_B[63]);
  assign sig_tmp[2] = (zero_A[2] | zero_B[2]) ? ((op_A[47] ^ op_B[47]) & (op_C[47] & zero_C[2])) : (op_A[47] ^ op_B[47]);
  assign sig_tmp[1] = (zero_A[1] | zero_B[1]) ? ((op_A[31] ^ op_B[31]) & (op_C[31] & zero_C[1])) : (op_A[31] ^ op_B[31]);
  assign sig_tmp[0] = (zero_A[0] | zero_B[0]) ? ((op_A[15] ^ op_B[15]) & (op_C[15] & zero_C[0])) : (op_A[15] ^ op_B[15]);
  assign exp_tmp    = {exp_tmp_3, exp_tmp_2, exp_tmp_1, exp_tmp_0};

endmodule