module NormShifter(
  input   [1:0]       prcn,
  input   [162:0]     sum_comp,
  input   [22:0]      lzac_cnt,
  input   [3:0]       lzac_vld,
  input   [36:0]      exp_tmp,
  input   [26:0]      sh_amt,
  output  [55:0]      sum_norm,
  output  [28:0]      exp_norm,
  output  [3:0]       of_flag,
  input   [3:0]       subtract
);

  wire  [7:0]       sh_const_3;
  wire  [5:0]       sh_const_2;
  wire  [6:0]       sh_const_1;
  wire  [5:0]       sh_const_0;

  wire  [12:0]      sh_amt_s1_3, exp_norm_s1_3, exp_norm_s2_3, exp_norm_s3_3, exp_norm_s4_3;
  wire  [6:0]       sh_amt_s1_2, exp_norm_s1_2, exp_norm_s2_2, exp_norm_s3_2, exp_norm_s4_2;
  wire  [9:0]       sh_amt_s1_1, exp_norm_s1_1, exp_norm_s2_1, exp_norm_s3_1, exp_norm_s4_1;
  wire  [6:0]       sh_amt_s1_0, exp_norm_s1_0, exp_norm_s2_0, exp_norm_s3_0, exp_norm_s4_0;

  wire  [6:0]       sh_amt_s2_3;
  wire  [4:0]       sh_amt_s2_2;
  wire  [5:0]       sh_amt_s2_1;
  wire  [4:0]       sh_amt_s2_0;

  wire  [3:0]       sh_en_s1;
  wire  [162:0]     sum_comp_s1, sum_comp_s1_d, sum_comp_s1_s, sum_comp_s1_h;
  wire  [3:0]       corr_s1;
  wire  [3:0]       sh_less_s1, sh_less_s1_0;

  wire  [22:0]      sh_amt_s2, sh_amt_s2_d, sh_amt_s2_s, sh_amt_s2_h;
  wire  [162:0]     sum_comp_s2;
  wire  [3:0]       data_left_s2;
  
  wire  [162:0]     sum_comp_s3, sum_comp_s3_d, sum_comp_s3_s, sum_comp_s3_h;
  wire  [3:0]       bypass_s3;
  wire  [3:0]       subnorm_s3, subnorm_s3_0;

  wire  [162:0]     sum_comp_s4, sum_comp_s4_d, sum_comp_s4_s, sum_comp_s4_h;
  wire  [3:0]       corr_s4, corr_s4_0;
  wire              sh_in_s4_3, sh_in_s4_2, sh_in_s4_1;

  wire  [55:0]      sum_norm_d, sum_norm_s, sum_norm_h;

  wire  [7:0]       amt_mask_3;
  wire  [5:0]       amt_mask_2;
  wire  [6:0]       amt_mask_1;
  wire  [5:0]       amt_mask_0;

  // exponent
  ExpNorm #(
    .EXP_WIDTH      (13),
    .AMT_WIDTH      (8)
  )
  u_ExpNorm_3(
    .exp_tmp        (exp_tmp[36:24]),
    .sh_amt         (sh_amt[26:19]),
    .sh_const       (sh_const_3),
    .sh_amt_s1      (sh_amt_s1_3),
    .lzac_cnt       (lzac_cnt[22:16]),
    .lzac_vld       (lzac_vld[3]),
    .exp_norm       (exp_norm_s1_3),
    .sh_en_s1       (sh_en_s1[3]),
    .sh_amt_s2      (sh_amt_s2_3),
    .subtract       (subtract[3]),
    .sh_less_s1     (sh_less_s1_0[3]),
    .amt_mask       (amt_mask_3)
  );

  ExpNorm #(
    .EXP_WIDTH      (7),
    .AMT_WIDTH      (6)
  )
  u_ExpNorm_2(
    .exp_tmp        (exp_tmp[23:17]),
    .sh_amt         (sh_amt[18:13]),
    .sh_const       (sh_const_2),
    .sh_amt_s1      (sh_amt_s1_2),
    .lzac_cnt       (lzac_cnt[15:11]),
    .lzac_vld       (lzac_vld[2]),
    .exp_norm       (exp_norm_s1_2),
    .sh_en_s1       (sh_en_s1[2]),
    .sh_amt_s2      (sh_amt_s2_2),
    .subtract       (subtract[2]),
    .sh_less_s1     (sh_less_s1_0[2]),
    .amt_mask       (amt_mask_2)
  );

  ExpNorm #(
    .EXP_WIDTH      (10),
    .AMT_WIDTH      (7)
  )
  u_ExpNorm_1(
    .exp_tmp        (exp_tmp[16:7]),
    .sh_amt         (sh_amt[12:6]),
    .sh_const       (sh_const_1),
    .sh_amt_s1      (sh_amt_s1_1),
    .lzac_cnt       (lzac_cnt[10:5]),
    .lzac_vld       (lzac_vld[1]),
    .exp_norm       (exp_norm_s1_1),
    .sh_en_s1       (sh_en_s1[1]),
    .sh_amt_s2      (sh_amt_s2_1),
    .subtract       (subtract[1]),
    .sh_less_s1     (sh_less_s1_0[1]),
    .amt_mask       (amt_mask_1)
  );

  ExpNorm #(
    .EXP_WIDTH      (7),
    .AMT_WIDTH      (6)
  )
  u_ExpNorm_0(
    .exp_tmp        (exp_tmp[6:0]),
    .sh_amt         (sh_amt[5:0]),
    .sh_const       (sh_const_0),
    .sh_amt_s1      (sh_amt_s1_0),
    .lzac_cnt       (lzac_cnt[4:0]),
    .lzac_vld       (lzac_vld[0]),
    .exp_norm       (exp_norm_s1_0),
    .sh_en_s1       (sh_en_s1[0]),
    .sh_amt_s2      (sh_amt_s2_0),
    .subtract       (subtract[0]),
    .sh_less_s1     (sh_less_s1_0[0]),
    .amt_mask       (amt_mask_0)
  );

  ConstShifter u_ConstShifter(
    .prcn           (prcn),
    .sh_en          (sh_en_s1),
    .data_in        (sum_comp),
    .data_out       (sum_comp_s1),
    .exp_corr       (corr_s1),
    .sh_less        (sh_less_s1)
  );

  LeftShifter u_LeftShifter(
    .prcn           (prcn),
    .data_in        (sum_comp_s1),
    .sh_amt         (sh_amt_s2),
    .data_out       (sum_comp_s2),
    .data_left      (data_left_s2)
  );

  // exponent
  assign sh_const_3 = prcn[1] ? 8'd54 : (prcn[0] ? 8'd25 : 8'd12);
  assign sh_const_2 = 6'd12;
  assign sh_const_1 = prcn[0] ? 7'd25 : 7'd12;
  assign sh_const_0 = 6'd12;

  assign sh_amt_s1_3 = prcn[1] ? 13'd8137 : (prcn[0] ? 13'd8166 : 13'd8179);
  assign sh_amt_s1_2 = 7'd115;
  assign sh_amt_s1_1 = prcn[0] ? 10'd998 : 10'd1011;
  assign sh_amt_s1_0 = 7'd115;

  assign sh_less_s1[3] = sh_less_s1_0[3];
  assign sh_less_s1[2] = (prcn==2'b00) ? sh_less_s1_0[2] : sh_less_s1_0[3];
  assign sh_less_s1[1] = prcn[1] ? sh_less_s1_0[3] : sh_less_s1_0[1];
  assign sh_less_s1[0] = prcn[1] ? sh_less_s1_0[3] : (prcn[0] ? sh_less_s1_0[1] : sh_less_s1_0[0]);

  assign amt_mask_3 = prcn[1] ? 8'h80 : (prcn[0] ? 8'h40 : 8'h20);
  assign amt_mask_2 = 6'h20;
  assign amt_mask_1 = prcn[0] ? 7'h40 : 7'h20;
  assign amt_mask_0 = 6'h20;

  // correction: due to carry out in addend, the const shifter can have 1-bit error
  assign exp_norm_s2_3 = (corr_s1[3] & sh_en_s1[3]) ? exp_norm_s1_3+1 : exp_norm_s1_3;
  assign exp_norm_s2_2 = (corr_s1[2] & sh_en_s1[2]) ? exp_norm_s1_2+1 : exp_norm_s1_2;
  assign exp_norm_s2_1 = (corr_s1[1] & sh_en_s1[1]) ? exp_norm_s1_1+1 : exp_norm_s1_1;
  assign exp_norm_s2_0 = (corr_s1[0] & sh_en_s1[0]) ? exp_norm_s1_0+1 : exp_norm_s1_0;

  // Second stage shifter
  assign sh_amt_s2_d  = {sh_amt_s2_3, sh_amt_s2_3[4:0], sh_amt_s2_3[5:0], sh_amt_s2_3[4:0]};
  assign sh_amt_s2_s  = {sh_amt_s2_3, sh_amt_s2_3[4:0], sh_amt_s2_1,      sh_amt_s2_1[4:0]};
  assign sh_amt_s2_h  = {sh_amt_s2_3, sh_amt_s2_2,      sh_amt_s2_1,      sh_amt_s2_0     };
  assign sh_amt_s2    = prcn[1] ? sh_amt_s2_d : (prcn[0] ? sh_amt_s2_s : sh_amt_s2_h);

  // correction: due to the carry out in addend, the actual MSB can have +1/-1 bit error
  assign bypass_s3[3] = (exp_norm_s2_3 == 13'd0);
  assign bypass_s3[2] = (prcn==2'b00) ? (exp_norm_s2_2 == 7'd0) : bypass_s3[3];
  assign bypass_s3[1] = prcn[1] ? bypass_s3[3] : (exp_norm_s2_1 == 10'd0);
  assign bypass_s3[0] = prcn[1] ? bypass_s3[3] : (prcn[0] ? (exp_norm_s2_1 == 10'd0) : (exp_norm_s2_0 == 7'd0));
  
  assign subnorm_s3_0[3]  = (exp_norm_s2_3 == 13'd1) & ~data_left_s2[3] & ~sum_comp_s2[162];
  assign subnorm_s3_0[2]  = (exp_norm_s2_2 ==  7'd1) & ~data_left_s2[2] & ~sum_comp_s2[120];
  assign subnorm_s3_0[1]  = (exp_norm_s2_1 == 10'd1) & ~data_left_s2[1] & ~sum_comp_s2[80];
  assign subnorm_s3_0[0]  = (exp_norm_s2_0 ==  7'd1) & ~data_left_s2[0] & ~sum_comp_s2[39];

  assign subnorm_s3[3] = subnorm_s3_0[3];
  assign subnorm_s3[2] = (prcn==2'b00) ? subnorm_s3_0[2] : subnorm_s3[3];
  assign subnorm_s3[1] = prcn[1] ? subnorm_s3[3] : subnorm_s3_0[1];
  assign subnorm_s3[0] = prcn[1] ? subnorm_s3[3] : (prcn[0] ? subnorm_s3_0[1] : subnorm_s3_0[0]);

  assign sum_comp_s3_d          = data_left_s2[3] ? {1'd1, sum_comp_s2[162:1]}   : (sum_comp_s2[162] ? sum_comp_s2[162:0]   : {sum_comp_s2[161:0], 1'd0}  );
  assign sum_comp_s3_s[162:81]  = data_left_s2[3] ? {1'd1, sum_comp_s2[162:82]}  : (sum_comp_s2[162] ? sum_comp_s2[162:81]  : {sum_comp_s2[161:81], 1'd0} );
  assign sum_comp_s3_s[80:0]    = data_left_s2[1] ? {1'd1, sum_comp_s2[80:1]}    : (sum_comp_s2[80]  ? sum_comp_s2[80:0]    : {sum_comp_s2[79:0], 1'd0}   );
  assign sum_comp_s3_h[162:121] = data_left_s2[3] ? {1'd1, sum_comp_s2[162:122]} : (sum_comp_s2[162] ? sum_comp_s2[162:121] : {sum_comp_s2[162:121], 1'd0});
  assign sum_comp_s3_h[120:81]  = data_left_s2[2] ? {1'd1, sum_comp_s2[120:82]}  : (sum_comp_s2[120] ? sum_comp_s2[120:81]  : {sum_comp_s2[119:81], 1'd0} );
  assign sum_comp_s3_h[80:40]   = data_left_s2[1] ? {1'd1, sum_comp_s2[80:41]}   : (sum_comp_s2[80]  ? sum_comp_s2[80:40]   : {sum_comp_s2[79:40], 1'd0}  );
  assign sum_comp_s3_h[39:0]    = data_left_s2[0] ? {1'd1, sum_comp_s2[39:1]}    : (sum_comp_s2[39]  ? sum_comp_s2[39:0]    : {sum_comp_s2[38:0], 1'd0}   );
  assign sum_comp_s3[162:121]   = bypass_s3[3]|subnorm_s3[3] ? sum_comp_s2[162:121] : (prcn[1] ? sum_comp_s3_d[162:121] : (prcn[0] ? sum_comp_s3_s[162:121] : sum_comp_s3_h[162:121]));
  assign sum_comp_s3[120:81]    = bypass_s3[2]|subnorm_s3[2] ? sum_comp_s2[120:81]  : (prcn[1] ? sum_comp_s3_d[120:81]  : (prcn[0] ? sum_comp_s3_s[120:81]  : sum_comp_s3_h[120:81] ));
  assign sum_comp_s3[80:40]     = bypass_s3[1]|subnorm_s3[1] ? sum_comp_s2[80:40]   : (prcn[1] ? sum_comp_s3_d[80:40]   : (prcn[0] ? sum_comp_s3_s[80:40]   : sum_comp_s3_h[80:40]  ));
  assign sum_comp_s3[39:0]      = bypass_s3[0]|subnorm_s3[0] ? sum_comp_s2[39:0]    : (prcn[1] ? sum_comp_s3_d[39:0]    : (prcn[0] ? sum_comp_s3_s[39:0]    : sum_comp_s3_h[39:0]   ));

  assign exp_norm_s3_3  = subnorm_s3[3] ? 13'd0 : (bypass_s3[3] ? exp_norm_s2_3 : (data_left_s2[3] ? exp_norm_s2_3+1 : (sum_comp_s2[162] ? exp_norm_s2_3 : exp_norm_s2_3-1)));
  assign exp_norm_s3_2  = subnorm_s3[2] ?  7'd0 : (bypass_s3[2] ? exp_norm_s2_2 : (data_left_s2[2] ? exp_norm_s2_2+1 : (sum_comp_s2[120] ? exp_norm_s2_2 : exp_norm_s2_2-1))); 
  assign exp_norm_s3_1  = subnorm_s3[1] ? 10'd0 : (bypass_s3[1] ? exp_norm_s2_1 : (data_left_s2[1] ? exp_norm_s2_1+1 : (sum_comp_s2[80]  ? exp_norm_s2_1 : exp_norm_s2_1-1))); 
  assign exp_norm_s3_0  = subnorm_s3[0] ?  7'd0 : (bypass_s3[0] ? exp_norm_s2_0 : (data_left_s2[0] ? exp_norm_s2_0+1 : (sum_comp_s2[39]  ? exp_norm_s2_0 : exp_norm_s2_0-1))); 

  // correction: one-bit error in lzac
  assign corr_s4_0[3] = lzac_vld[3] & ~sum_comp_s3[162] & (exp_norm_s3_3 > 13'd1);
  assign corr_s4_0[2] = lzac_vld[2] & ~sum_comp_s3[120] & (exp_norm_s3_2 > 7'd1 );
  assign corr_s4_0[1] = lzac_vld[1] & ~sum_comp_s3[80]  & (exp_norm_s3_1 > 10'd1);
  assign corr_s4_0[0] = lzac_vld[0] & ~sum_comp_s3[39]  & (exp_norm_s3_0 > 7'd1 );
  assign corr_s4[3]   = corr_s4_0[3];
  assign corr_s4[2]   = (prcn==2'b00) ? corr_s4_0[2] : corr_s4_0[3];
  assign corr_s4[1]   = prcn[1] ? corr_s4_0[3] : corr_s4_0[1];
  assign corr_s4[0]   = prcn[1] ? corr_s4_0[3] : (prcn[0] ? corr_s4_0[1] : corr_s4_0[0]);

  assign sh_in_s4_3 = (prcn==2'b00) ? 1'd0 : sum_comp_s3[120];
  assign sh_in_s4_2 = prcn[1] ? sum_comp_s3[80] : 1'd0;
  assign sh_in_s4_1 = (prcn==2'b00) ? 1'd0 : sum_comp_s3[39];
  assign sum_comp_s4[162:121] = corr_s4[3] ? {sum_comp_s3[161:121], sh_in_s4_3} : sum_comp_s3[162:121];
  assign sum_comp_s4[120:81]  = corr_s4[2] ? {sum_comp_s3[119:81] , sh_in_s4_2} : sum_comp_s3[120:81];
  assign sum_comp_s4[80:40]   = corr_s4[1] ? {sum_comp_s3[79:40]  , sh_in_s4_1} : sum_comp_s3[80:40];
  assign sum_comp_s4[39:0]    = corr_s4[0] ? {sum_comp_s3[38:0]   , 1'd0}       : sum_comp_s3[39:0];

  assign exp_norm_s4_3 = corr_s4[3] ? exp_norm_s3_3-1 : exp_norm_s3_3;
  assign exp_norm_s4_2 = corr_s4[2] ? exp_norm_s3_2-1 : exp_norm_s3_2;
  assign exp_norm_s4_1 = corr_s4[1] ? exp_norm_s3_1-1 : exp_norm_s3_1;
  assign exp_norm_s4_0 = corr_s4[0] ? exp_norm_s3_0-1 : exp_norm_s3_0;

  // output
  assign sum_norm_d[55:1]   = sum_comp_s4[162:108];
  assign sum_norm_d[0]      = |sum_comp_s4[107:0];
  assign sum_norm_s[55:29]  = {1'd0, sum_comp_s4[162:137]};
  assign sum_norm_s[28]     = |sum_comp_s4[136:81];
  assign sum_norm_s[27:1]   = {1'd0, sum_comp_s4[80:55]};
  assign sum_norm_s[0]      = |sum_comp_s4[54:0];
  assign sum_norm_h[55:43]  = sum_comp_s4[162:150];
  assign sum_norm_h[42]     = |sum_comp_s4[149:121];
  assign sum_norm_h[41:29]  = sum_comp_s4[120:108];
  assign sum_norm_h[28]     = |sum_comp_s4[107:81];
  assign sum_norm_h[27:15]  = sum_comp_s4[80:68];
  assign sum_norm_h[14]     = |sum_comp_s4[67:40];
  assign sum_norm_h[13:1]   = sum_comp_s4[39:27];
  assign sum_norm_h[0]      = |sum_comp_s4[26:0]; 
  assign sum_norm           = prcn[1] ? sum_norm_d : (prcn[0] ? sum_norm_s : sum_norm_h);

  assign exp_norm = {exp_norm_s4_3[10:0], exp_norm_s4_2[4:0], exp_norm_s4_1[7:0], exp_norm_s4_0[4:0]};

  assign of_flag[3] = prcn[1] ? (exp_norm_s4_3>=13'h7FF) : (prcn[0] ? (exp_norm_s4_3>=13'hFF) : (exp_norm_s4_3>= 13'h1F));
  assign of_flag[2] = (exp_norm_s4_2>=7'h1F);
  assign of_flag[1] = prcn[0] ? (exp_norm_s4_1>=10'hFF) : (exp_norm_s4_1>=10'h1F);
  assign of_flag[0] = exp_norm_s4_0>=7'h1F;

endmodule