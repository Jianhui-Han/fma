module RndUnit #(
  parameter MANT_WIDTH  = 52,
  parameter EXP_WIDTH   = 11
)(
  input   [2:0]             rnd_mode,
  input                     sig_fma,
  input   [EXP_WIDTH-1:0]   exp_norm,
  input   [MANT_WIDTH+3:0]  sum_norm,
  output  [MANT_WIDTH-1:0]  mant_rnd,
  output  [EXP_WIDTH-1:0]   exp_fma,
  input                     sticky,
  input                     subtract
);

  wire  [2:0]                 grs, grs_minus, grs_plus;
  wire                        rne_up;
  wire  [MANT_WIDTH+1:0]      mant_rne, mant_rtz, mant_rdn, mant_rup, mant_rmm;
  wire  [MANT_WIDTH+1:0]      mant_rnd_raw;
  wire                        subnorm;

  assign grs_minus  = (|sum_norm[2:0]) ? sum_norm[2:0]-1 : sum_norm[2:0];
  assign grs_plus   = (&sum_norm[2:0]) ? sum_norm[2:0] : sum_norm[2:0]+1;
  assign grs        = (~sticky | (sticky & sum_norm[0])) ? sum_norm[2:0] : (subtract ? grs_minus : grs_plus);
  assign rne_up     = (grs > 3'b100) | ((grs == 3'b100) & mant_rtz[0]);

  assign mant_rne = rne_up ? mant_rtz+1 : mant_rtz;
  assign mant_rtz = {1'd0, sum_norm[MANT_WIDTH+3:3]};
  assign mant_rdn = (sig_fma & (grs!=3'b000)) ? mant_rtz+1 : mant_rtz;
  assign mant_rup = (~sig_fma & (grs!=3'b000)) ? mant_rtz+1 : mant_rtz;
  assign mant_rmm = grs[2] ? mant_rtz+1 : mant_rtz;

  assign mant_rnd_raw_0 = rnd_mode[0] ? mant_rup : mant_rdn;
  assign mant_rnd_raw_1 = rnd_mode[2] ? mant_rne : mant_rtz; 
  assign mant_rnd_raw_2 = (rnd_mode[2:1] == 2'b01) ? mant_rnd_raw_0 : mant_rnd_raw_1;
  assign mant_rnd_raw_3 = (rnd_mode == 3'b100) ? mant_rmm : mant_rnd_raw_2; 
  assign mant_rnd_raw   = (rnd_mode == 3'b000) ? mant_rne : mant_rnd_raw_3;
  assign mant_rnd       = mant_rnd_raw[MANT_WIDTH-1:0];
  assign subnorm        = ~sum_norm[MANT_WIDTH+3];
  assign exp_fma        = (mant_rnd_raw[MANT_WIDTH+1] | (subnorm & mant_rnd_raw[MANT_WIDTH])) ? exp_norm+1 : exp_norm;

endmodule