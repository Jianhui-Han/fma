module Multiplier15(
  input               en,
  input   [14:0]      op_A,
  input   [14:0]      op_B,
  output  [29:0]      product
);
  
  wire    [16:0]      op_B_ext;
  wire    [127:0]     pp_body;
  wire    [7:0]       pp_sign;
  wire    [239:0]     csa_0_op;
  wire    [59:0]      csa_0_sum;
  wire    [59:0]      csa_0_carry;
  wire    [29:0]      csa_1_sum;
  wire    [29:0]      csa_1_carry;

  genvar i;
  generate
    for (i=0; i<8; i=i+1) begin : booth
      BoothEnc u_BoothEnc(
        .op_A       (op_A),
        .op_B       (op_B_ext[i*2+2:i*2]),
        .pp_body    (pp_body[i*16+15:i*16]),
        .pp_sign    (pp_sign[i])
      );
    end

    for (i=0; i<2; i=i+1) begin : csa_0
      CSA42 #(
        .WIDTH      (30)
      )
      u_CSA42_0(
        .op_A       (csa_0_op[i*120+29:i*120]),
        .op_B       (csa_0_op[i*120+59:i*120+30]),
        .op_C       (csa_0_op[i*120+89:i*120+60]),
        .op_D       (csa_0_op[i*120+119:i*120+90]),
        .sum        (csa_0_sum[i*30+29:i*30]),
        .carry      (csa_0_carry[i*30+29:i*30])
      );
    end
  endgenerate

  CSA42 #(
    .WIDTH      (30)
  )
  u_CSA42_1(
    .op_A       (csa_0_sum[29:0]),
    .op_B       (csa_0_sum[59:30]),
    .op_C       ({csa_0_carry[28:0], 1'd0}),
    .op_D       ({csa_0_carry[58:30], 1'd0}),
    .sum        (csa_1_sum),
    .carry      (csa_1_carry)
  );
 
  assign op_B_ext = {1'b0, op_B, 1'b0};

  assign csa_0_op[239:210]  = {pp_body[127:112], 1'd0, pp_sign[6], 12'd0};
  assign csa_0_op[209:180]  = {1'd1, ~pp_sign[6], pp_body[111:96], 1'd0, pp_sign[5], 10'd0};
  assign csa_0_op[179:150]  = {3'd1, ~pp_sign[5], pp_body[95:80], 1'd0, pp_sign[4], 8'd0};
  assign csa_0_op[149:120]  = {5'd1, ~pp_sign[4], pp_body[79:64], 1'd0, pp_sign[3], 6'd0};
  assign csa_0_op[119:90]   = {7'd1, ~pp_sign[3], pp_body[63:48], 1'd0, pp_sign[2], 4'd0};
  assign csa_0_op[89:60]    = {9'd1, ~pp_sign[2], pp_body[47:32], 1'd0, pp_sign[1], 2'd0};
  assign csa_0_op[59:30]    = {11'd1, ~pp_sign[1], pp_body[31:16], 1'd0, pp_sign[0]};
  assign csa_0_op[29:0]     = {11'd0, ~pp_sign[0], pp_sign[0], pp_sign[0], pp_body[15:0]};

  assign product = en ? {1'd0, csa_1_sum} + {csa_1_carry, 1'd0} : 30'd0;

endmodule