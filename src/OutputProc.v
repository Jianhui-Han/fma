module OutputProc(
  input   [1:0]     prcn,
  input   [51:0]    mant_fma,
  input   [28:0]    exp_fma,
  input   [3:0]     sig_fma,
  output  [63:0]    res_fma,
  input   [3:0]     of_flag,
  input   [3:0]     nan_flag,
  input   [63:0]    nan_res,
  input   [3:0]     inf_flag,
  input   [63:0]    inf_res
);

  wire    [63:0]    res_fma_d_raw, res_fma_s_raw, res_fma_h_raw;
  wire    [63:0]    res_fma_d_of, res_fma_s_of, res_fma_h_of;
  wire    [63:0]    res_fma_cal, res_fma_d_cal, res_fma_s_cal, res_fma_h_cal;

  assign res_fma_d_raw        = {sig_fma[3], exp_fma[28:18], mant_fma       };
  assign res_fma_s_raw[63:32] = {sig_fma[3], exp_fma[25:18], mant_fma[48:26]};
  assign res_fma_s_raw[31:0]  = {sig_fma[1], exp_fma[12:5],  mant_fma[22:0] };
  assign res_fma_h_raw[63:48] = {sig_fma[3], exp_fma[22:18], mant_fma[48:39]};
  assign res_fma_h_raw[47:32] = {sig_fma[2], exp_fma[17:13], mant_fma[35:26]};
  assign res_fma_h_raw[31:16] = {sig_fma[1], exp_fma[9:5],   mant_fma[22:13]};
  assign res_fma_h_raw[15:0]  = {sig_fma[0], exp_fma[4:0],   mant_fma[9:0]  };

  assign res_fma_d_of         = {sig_fma[3], 63'h7FF0000000000000};
  assign res_fma_s_of[63:32]  = {sig_fma[3], 31'h7F800000};
  assign res_fma_s_of[31:0]   = {sig_fma[1], 31'h7F800000};
  assign res_fma_h_of[63:48]  = {sig_fma[3], 15'h7C00};
  assign res_fma_h_of[47:32]  = {sig_fma[2], 15'h7C00};
  assign res_fma_h_of[31:16]  = {sig_fma[1], 15'h7C00};
  assign res_fma_h_of[15:0]   = {sig_fma[0], 15'h7C00};

  assign res_fma_d_cal        = of_flag[3] ? res_fma_d_of        : res_fma_d_raw;
  assign res_fma_s_cal[63:32] = of_flag[3] ? res_fma_s_of[63:32] : res_fma_s_raw[63:32];
  assign res_fma_s_cal[31:0]  = of_flag[1] ? res_fma_s_of[31:0]  : res_fma_s_raw[31:0];
  assign res_fma_h_cal[63:48] = of_flag[3] ? res_fma_h_of[63:48] : res_fma_h_raw[63:48];
  assign res_fma_h_cal[47:32] = of_flag[2] ? res_fma_h_of[47:32] : res_fma_h_raw[47:32];
  assign res_fma_h_cal[31:16] = of_flag[1] ? res_fma_h_of[31:16] : res_fma_h_raw[31:16];
  assign res_fma_h_cal[15:0]  = of_flag[0] ? res_fma_h_of[15:0]  : res_fma_h_raw[15:0];
  assign res_fma_cal          = prcn[1] ? res_fma_d_cal : (prcn[0] ? res_fma_s_cal : res_fma_h_cal);

  assign res_fma[63:48] = nan_flag[3] ? nan_res[63:48] : (inf_flag[3] ? inf_res[63:48] : res_fma_cal[63:48]);
  assign res_fma[47:32] = nan_flag[2] ? nan_res[47:32] : (inf_flag[2] ? inf_res[47:32] : res_fma_cal[47:32]);
  assign res_fma[31:16] = nan_flag[1] ? nan_res[31:16] : (inf_flag[1] ? inf_res[31:16] : res_fma_cal[31:16]);
  assign res_fma[15:0]  = nan_flag[0] ? nan_res[15:0]  : (inf_flag[0] ? inf_res[15:0]  : res_fma_cal[15:0] );

endmodule