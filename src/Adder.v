module Adder(
  input   [1:0]         prcn,
  input   [2:0]         rnd_mode,
  input   [107:0]       prod_sum,
  input   [107:0]       prod_carry,
  input   [162:0]       addend,
  input   [3:0]         sig_tmp,
  input   [3:0]         subtract,
  output  [162:0]       sum_comp,
  output  [3:0]         sig_fma,
  output  [22:0]        lzac_cnt,
  output  [3:0]         lzac_vld
);

  wire    [107:0]   csa_sum, csa_carry;
  wire    [108:0]   sum_low, csa_comp, csa_comp_d, csa_comp_s, csa_comp_h;
  wire    [107:0]   addend_low, addend_low_d, addend_low_s, addend_low_h;

  wire    [3:0]     carry_out_low;
  wire    [54:0]    addend_high, addend_high_d, addend_high_s, addend_high_h;
  wire    [15:0]    sum_high_3;
  wire    [13:0]    sum_high_2;
  wire    [14:0]    sum_high_1;
  wire    [13:0]    sum_high_0;

  wire    [162:0]   sum_all, sum_all_inv, sum_all_d, sum_all_s, sum_all_h;
  wire    [162:0]   sum_comp_d, sum_comp_s, sum_comp_h;

  wire    [2:0]     lzac_concat;
  wire    [3:0]     lzac_zero, lzac_zero_d, lzac_zero_s, lzac_zero_h;

  wire    [3:0]     sig_sum, sig_comp;
  wire              sig_fma_d;
  wire    [1:0]     sig_fma_s;
  wire    [3:0]     sig_fma_h;

  CSA32 #(
    .WIDTH      (108)
  )
  u_CSA32(
    .op_A       (prod_sum),
    .op_B       ({prod_carry[106:0], 1'd0}),
    .op_C       (addend_low),
    .sum        (csa_sum),
    .carry      (csa_carry)
  );

  LZAC u_LZAC(
    .prcn       (prcn),
    .sum        (csa_sum),
    .carry      (csa_comp[107:0]),
    .concat     (lzac_concat),
    .zero       (lzac_zero),
    .count      (lzac_cnt),
    .valid      (lzac_vld)
  );

  // csa
  assign addend_low_d = addend[107:0];
  assign addend_low_s = {addend[130:81], 4'd0, addend[49:0], 4'd0};
  assign addend_low_h = {addend[144:121], 3'd0, addend[104:81], 3'd0, addend[63:40], 3'd0, addend[23:0], 3'd0};
  assign addend_low   = prcn[1] ? addend_low_d : (prcn[0] ? addend_low_s : addend_low_h);

  assign csa_comp_d         = {csa_carry[107:0],  subtract[3]};
  assign csa_comp_s[108:58] = {csa_carry[107:58], subtract[3]};
  assign csa_comp_s[57:0]   = {csa_carry[56:4],   subtract[1], 4'd0};
  assign csa_comp_h[108:84] = {csa_carry[107:84], subtract[3]};
  assign csa_comp_h[83:57]  = {csa_carry[82:57],  subtract[2]};
  assign csa_comp_h[56:30]  = {csa_carry[55:30],  subtract[1]};
  assign csa_comp_h[29:0]   = {csa_carry[28:3],   subtract[0], 3'd0};
  assign csa_comp           = prcn[1] ? csa_comp_d : (prcn[0] ? csa_comp_s : csa_comp_h);
  
  assign sum_low = csa_sum + csa_comp;
  
  // adder for sum_high
  assign addend_high_d    = addend[162:108];
  assign addend_high_s    = {addend[158:131], addend[76:50]};
  assign addend_high_h    = {addend[159:145], addend[117:105], addend[77:64], addend[36:24]};
  assign addend_high      = prcn[1] ? addend_high_d : (prcn[0] ? addend_high_s : addend_high_h);
  assign carry_out_low[3] = (prcn==2'b00) ? sum_low[108] : sum_high_2[13];
  assign carry_out_low[2] = prcn[1] ? sum_high_1[14] : (prcn[0] ? sum_low[108] : sum_low[81]);
  assign carry_out_low[1] = (prcn==2'b00) ? sum_low[54] : sum_high_0[13];
  assign carry_out_low[0] = prcn[1] ? sum_low[108] : (prcn[0] ? sum_low[54] : sum_low[27]);
  assign sum_high_3       = carry_out_low[3] ? addend_high[54:40]+1 : addend_high[54:40];
  assign sum_high_2       = carry_out_low[2] ? addend_high[39:27]+1 : addend_high[39:27];
  assign sum_high_1       = carry_out_low[1] ? addend_high[26:13]+1 : addend_high[26:13];
  assign sum_high_0       = carry_out_low[0] ? addend_high[12:0]+1  : addend_high[12:0];

  // sign & complement 
  assign sig_sum      = {sum_high_3[15], sum_high_2[13], sum_high_1[14], sum_high_0[13]};
  assign sig_comp     = sig_sum ^ subtract;
  assign sig_fma_d    = ((sum_comp_d          == 163'd0) & sig_sum[3]) ? (rnd_mode == 3'd2) : (sig_tmp[3] ^ sig_comp[3]);
  assign sig_fma_s[1] = ((sum_comp_s[162:81]  ==  82'd0) & sig_sum[3]) ? (rnd_mode == 3'd2) : (sig_tmp[3] ^ sig_comp[3]);
  assign sig_fma_s[0] = ((sum_comp_s[80:0]    ==  81'd0) & sig_sum[1]) ? (rnd_mode == 3'd2) : (sig_tmp[1] ^ sig_comp[1]);
  assign sig_fma_h[3] = ((sum_comp_h[162:121] ==  42'd0) & sig_sum[3]) ? (rnd_mode == 3'd2) : (sig_tmp[3] ^ sig_comp[3]);
  assign sig_fma_h[2] = ((sum_comp_h[120:81]  ==  40'd0) & sig_sum[2]) ? (rnd_mode == 3'd2) : (sig_tmp[2] ^ sig_comp[2]);
  assign sig_fma_h[1] = ((sum_comp_h[80:40]   ==  41'd0) & sig_sum[1]) ? (rnd_mode == 3'd2) : (sig_tmp[1] ^ sig_comp[1]);
  assign sig_fma_h[0] = ((sum_comp_h[39:0]    ==  40'd0) & sig_sum[0]) ? (rnd_mode == 3'd2) : (sig_tmp[0] ^ sig_comp[0]);
  assign sig_fma[3]   = prcn[1] ? sig_fma_d : (prcn[0] ? sig_fma_s[1] : sig_fma_h[3]);
  assign sig_fma[2]   = sig_fma_h[2];
  assign sig_fma[1]   = prcn[0] ? sig_fma_s[0] : sig_fma_h[1];
  assign sig_fma[0]   = sig_fma_h[0];

  assign sum_all_d            = {sum_high_3[14:0], sum_high_2[12:0], sum_high_1[13:0], sum_high_0[12:0], sum_low[107:0]};
  assign sum_all_s[162:81]    = {sum_high_3[12:0], sum_high_2[12:0], sum_low[107:58], 6'd0};
  assign sum_all_s[80:0]      = {sum_high_1[12:0], sum_high_0[12:0], sum_low[53:4], 5'd0};
  assign sum_all_h[162:121]   = {sum_high_3[12:0], sum_low[107:84], 5'd0};
  assign sum_all_h[120:81]    = {sum_high_2[12:0], sum_low[80:57], 3'd0};
  assign sum_all_h[80:40]     = {sum_high_1[12:0], sum_low[53:30], 4'd0};
  assign sum_all_h[39:0]      = {sum_high_0[12:0], sum_low[26:3], 3'd0};
  assign sum_all      = prcn[1] ? sum_all_d : (prcn[0] ? sum_all_s : sum_all_h);
  assign sum_all_inv  = ~sum_all;

  assign sum_comp_d           = sig_comp[3] ? sum_all_inv+1          : sum_all;
  assign sum_comp_s[162:81]   = sig_comp[3] ? sum_all_inv[162:81]+1  : sum_all[162:81];
  assign sum_comp_s[80:0]     = sig_comp[1] ? sum_all_inv[80:0]+1    : sum_all[80:0];
  assign sum_comp_h[162:121]  = sig_comp[3] ? sum_all_inv[162:121]+1 : sum_all[162:121];
  assign sum_comp_h[120:81]   = sig_comp[2] ? sum_all_inv[120:81]+1  : sum_all[120:81];
  assign sum_comp_h[80:40]    = sig_comp[1] ? sum_all_inv[80:40]+1   : sum_all[80:40];
  assign sum_comp_h[39:0]     = sig_comp[0] ? sum_all_inv[39:0]+1    : sum_all[39:0];
  assign sum_comp             = prcn[1] ? sum_comp_d : (prcn[0] ? sum_comp_s : sum_comp_h);

  // lzac
  assign lzac_concat      = {^sum_low[81:80], ^sum_low[54:53], ^sum_low[27:26]};
  assign lzac_zero_d[3]   = (&sum_high_0[1:0] & sum_low[107]) | ~(|sum_high_0[1:0] | sum_low[107]);
  assign lzac_zero_d[2:0] = 3'h7;
  assign lzac_zero_s[3]   = (&sum_high_2[1:0] & sum_low[107]) | ~(|sum_high_2[1:0] | sum_low[107]);
  assign lzac_zero_s[2]   = 1'd1;
  assign lzac_zero_s[1]   = (&sum_high_0[1:0] & sum_low[53]) | ~(|sum_high_0[1:0] | sum_low[53]);
  assign lzac_zero_s[0]   = 1'd1;
  assign lzac_zero_h[3]   = (&sum_high_3[1:0] & sum_low[107]) | ~(|sum_high_3[1:0] | sum_low[107]);
  assign lzac_zero_h[2]   = (&sum_high_2[1:0] & sum_low[80]) | ~(|sum_high_2[1:0] | sum_low[80]);
  assign lzac_zero_h[1]   = (&sum_high_1[1:0] & sum_low[53]) | ~(|sum_high_1[1:0] | sum_low[53]);
  assign lzac_zero_h[0]   = (&sum_high_0[1:0] & sum_low[26]) | ~(|sum_high_0[1:0] | sum_low[26]);
  assign lzac_zero        = ~(prcn[1] ? lzac_zero_d : (prcn[0] ? lzac_zero_s : lzac_zero_h));

endmodule