module Multiplier(
  input   [1:0]         prcn,
  input   [59:0]        op_A,
  input   [59:0]        op_B,
  output  [107:0]       prod_sum,
  output  [107:0]       prod_carry
);

  wire    [15:0]        mult15_en;
  wire    [479:0]       part_prod;

  wire    [359:0]       csa_0_out_sum, csa_0_out_carry;
  wire    [119:0]       csa_1_out_sum, csa_1_out_carry;

  wire    [107:0]       prod_sum_d, prod_sum_s, prod_sum_h;
  wire    [107:0]       prod_carry_d, prod_carry_s, prod_carry_h;

  genvar i, j;
  generate
    for (i=0; i<4; i=i+1) begin : loop_B
      for (j=0; j<4; j=j+1) begin : loop_A
        Multiplier15 u_Multiplier15_0(
          .en               (mult15_en[i*4+j]),
          .op_A             (op_A[j*15+14:j*15]),
          .op_B             (op_B[i*15+14:i*15]),
          .product          (part_prod[i*120+j*30+29:i*120+j*30])
        );
      end
    end

    for (i=0; i<2; i=i+1) begin : csa_0
      CSA42 #(
        .WIDTH      (90)
      )
      u_CSA42_0(
        .op_A       ({30'd0, part_prod[i*240+89:i*240+60], part_prod[i*240+29:i*240]}),
        .op_B       ({15'd0, part_prod[i*240+119:i*240+90], part_prod[i*240+59:i*240+30], 15'd0}),
        .op_C       ({15'd0, part_prod[i*240+209:i*240+180], part_prod[i*240+149:i*240+120], 15'd0}),
        .op_D       ({part_prod[i*240+239:i*240+210], part_prod[i*240+179:i*240+150], 30'd0}),
        .sum        (csa_0_out_sum[i*90+89:i*90]),
        .carry      (csa_0_out_carry[i*90+89:i*90])
      );
    end
  endgenerate

  CSA42 #(
    .WIDTH      (120)
  )
  u_CSA42_1 (
    .op_A       ({30'd0, csa_0_out_sum[89:0]}),
    .op_B       ({29'd0, csa_0_out_carry[89:0], 1'd0}),
    .op_C       ({csa_0_out_sum[179:90], 30'd0}),
    .op_D       ({csa_0_out_carry[178:90], 31'd0}),
    .sum        (csa_1_out_sum),
    .carry      (csa_1_out_carry)
  );

  // enable
  assign mult15_en = prcn[1] ? 16'hFFFF : (prcn[0] ? 16'hCC33 : 16'h8421);

  // output
  assign prod_sum_d   = {csa_1_out_sum[105:0], 2'd0};
  assign prod_sum_s   = {csa_1_out_sum[107:60], 6'd0, csa_1_out_sum[47:0], 6'd0};
  assign prod_sum_h   = {csa_1_out_sum[111:90], 5'd0, csa_1_out_sum[81:60], 5'd0, csa_1_out_sum[51:30], 5'd0, csa_1_out_sum[21:0], 5'd0};
  assign prod_carry_d = {csa_1_out_carry[105:0], 2'd0};
  assign prod_carry_s = {csa_1_out_carry[107:60], 6'd0, csa_1_out_carry[47:0], 6'd0};
  assign prod_carry_h = {csa_1_out_carry[111:90], 5'd0, csa_1_out_carry[81:60], 5'd0, csa_1_out_carry[51:30], 5'd0, csa_1_out_carry[21:0], 5'd0}; 
  assign prod_sum     = prcn[1] ? prod_sum_d : (prcn[0] ? prod_sum_s : prod_sum_h);
  assign prod_carry   = prcn[1] ? prod_carry_d : (prcn[0] ? prod_carry_s : prod_carry_h);

endmodule