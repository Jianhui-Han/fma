`timescale 1ns/1ns

module sim_tb;

  reg   [63:0]    mem[2559:0];

  reg             clk = 1'b0;
  reg             rst_n;
  reg             vld_in, vld_next;
  reg   [63:0]    op_A, op_B, op_C;
  reg   [63:0]    op_A_next, op_B_next, op_C_next;
  reg   [63:0]    res, res_d1, res_d2, res_d3, res_d4;
  reg   [1:0]     prcn, prcn_next;
  reg   [2:0]     rnd_mode, rnd_next;

  wire            vld_out;
  wire  [63:0]    result;
  wire            busy;

  reg   [8:0]     cnt_in, cnt_out, cnt_d1, cnt_d2;

  integer i, j;

  integer g_start, g_end;

  FMA u_FMA(
    .clk          (clk),
    .rst_n        (rst_n),
    .prcn         (prcn),
    .rnd_mode     (rnd_mode),
    .vld_in       (vld_in),
    .op_A         (op_A),
    .op_B         (op_B),
    .op_C         (op_C),
    .vld_out      (vld_out),
    .result       (result),
    .busy         (busy)
  );

  initial begin
    if (!$value$plusargs("start=%d", g_start))
      g_start = 0;
    if ($test$plusargs("debug"))
      g_end = g_start+1;
    else 
      g_end = 11979;

    if ($test$plusargs("dump")) begin
      $dumpfile("FMA.vcd");
      $dumpvars(0, sim_tb);
    end
  end

  // file
  initial begin
$display("=============================================================");
    $display("Simulation started.");
`ifdef f64
      $display("Testing: format f64 ...");
`elsif f32
      $display("Testing: format f32 ...");
`elsif f16
      $display("Testing: format f16 ...");
`else
      $display("Unknown format. Exiting ...");
      $finish
`endif
    op_A_next = 64'd0;
    op_B_next = 64'd0;
    op_C_next = 64'd0;
    res       = 64'd0;
    vld_next  = 1'd0;
    prcn_next = 2'b11;
    rnd_next  = 3'b000;
    #106
    for (i=g_start; i<g_end; i=i+1) begin
`ifdef f64
      $readmemh($sformatf("data/f64.data.%05d", i), mem);
      for (j=0; j<512; j=j+1) begin      
        op_A_next = mem[j*5+0];
        op_B_next = mem[j*5+1];
        op_C_next = mem[j*5+2];
        res       = mem[j*5+3];
        prcn_next = 2'b11;
`elsif f32
      $readmemh($sformatf("data/f32.data.%05d", i), mem);
      for (j=0; j<256; j=j+1) begin
        op_A_next = {mem[j*10+0][31:0], mem[j*10+5][31:0]};
        op_B_next = {mem[j*10+1][31:0], mem[j*10+6][31:0]};
        op_C_next = {mem[j*10+2][31:0], mem[j*10+7][31:0]};
        res       = {mem[j*10+3][31:0], mem[j*10+8][31:0]};
        prcn_next = 2'b01;
`elsif f16
      $readmemh($sformatf("data/f16.data.%05d", i), mem);
      for (j=0; j<128; j=j+1) begin
        op_A_next = {mem[j*20+0][15:0], mem[j*20+5][15:0], mem[j*20+10][15:0], mem[j*20+15][15:0]};
        op_B_next = {mem[j*20+1][15:0], mem[j*20+6][15:0], mem[j*20+11][15:0], mem[j*20+16][15:0]};
        op_C_next = {mem[j*20+2][15:0], mem[j*20+7][15:0], mem[j*20+12][15:0], mem[j*20+17][15:0]};
        res       = {mem[j*20+3][15:0], mem[j*20+8][15:0], mem[j*20+13][15:0], mem[j*20+18][15:0]};
        prcn_next = 2'b00;
`endif
        vld_next  = 1'd1;
        #10 ;
      end
      $display("Case group %8d passed.", i);
    end
    vld_next  = 1'd0;
    #100
    $display("All cases passed!");
    $display("=============================================================");
    $finish;
  end

  // env
  always #5 clk = ~clk;
  initial begin
    rst_n = 1'b0;
    #100
    rst_n = 1'b1;
  end

  // input
  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      vld_in  <= 1'd0;
      op_A    <= 64'd0;
      op_B    <= 64'd0;
      op_C    <= 64'd0;
      res_d4  <= 64'd0;
      res_d3  <= 64'd0;
      res_d2  <= 64'd0;
      res_d1  <= 64'd0;
      prcn    <= 2'd0;
      rnd_mode <= 3'd0;
    end
    else begin
      vld_in  <= vld_next;
      op_A    <= op_A_next;
      op_B    <= op_B_next;
      op_C    <= op_C_next;
      res_d4  <= res_d3;
      res_d3  <= res_d2;
      res_d2  <= res_d1;
      res_d1  <= res;
      prcn    <= prcn_next;
      rnd_mode <= rnd_next;
    end
  end

  always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      cnt_in  <= 9'd0;
      cnt_d1  <= 9'd0;
      cnt_d2  <= 9'd0;
      cnt_out <= 9'd0;
    end
    else begin
      cnt_out <= cnt_d2;
      cnt_d2  <= cnt_d1;
      cnt_d1  <= cnt_in;
      if (vld_in)
        cnt_in  <= cnt_in + 1; 
    end
  end
  // judge
  always @ (posedge clk) begin
    if (vld_out & (res_d4 !== result)) begin
      $display("Case %14d failed.", i*512+j-4);
      $display("=============================================================");
      $finish;
    end
  end

endmodule